/**********************************************************************
 *
 * kw/type/kwtCtrlPageMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

export class kwtCtrlPageMsg
{
    fltr: any;
    sArryIdD: string;
    sFltrIdD: string;
    sLinkIdD: string;
    sLinkTmpl: string;
    sStylIdD: string;
    sTextIdD: string;
}
