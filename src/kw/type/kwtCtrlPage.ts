/**********************************************************************
 *
 * kw/type/kwtCtrlComp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

export class kwtCtrlPage
{
    sId: string;
    sInitId: string;
    sStyle: string;
    sText: string;
    sLink: string;
}
