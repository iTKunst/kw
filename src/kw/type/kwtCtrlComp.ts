/**********************************************************************
 *
 * kw/type/kwtCtrlComp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

export class kwtCtrlComp
{
    sDataId: any;
    sId: string;
    sFltrId: any;
    sInitId: string;
	sStyle: string;
	sText: string;
    sLinkId: any;
    sLinkTmpl: string;
    sLink: string;
}
