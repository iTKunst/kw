/**********************************************************************
 *
 * kw/ctrl/kwCtrlPage.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off

import {kw}                         from "@kw/kw";
import {kwAttrs}                    from "@kwClass/attrs/kwAttrs";
import {kwDisp}                     from "@kwClass/disp/kwDisp";
import {kwDisp}                     from "@kwClass/disp/kwDisp";
import {kwLog}                      from "@kw/kwLog";
import {kwStDataVal}                from "@kwState/kwStDataVal";
import {kwTag}                      from "@kwClass/tag/kwTag";
import {kwtCtrlComp}                from "@kwType/kwtCtrlComp";




export abstract class kwCtrlPage
{
    protected sClass: string = this.constructor.name;

    public inits: any;
    public view: any;

    public sArry: string;
    public sComp: string;
    public sFltr: string;
    public sId: string;
    public sLink: string;
    public sStyl: string;
    public sText: string;

    private attrs: kwAttrs;
    private disp: kwDisp;
    private tag: kwTag;
    private type: kwtCtrlComp;

    private bDispId: boolean        = false;
    private bDispDisp: boolean      = false;
    private bDispInits: boolean     = false;
    private bDispTag: boolean       = false;
    private bDispView: boolean      = false;

    private bInit: boolean          = false;
    private bCreateAttrs: boolean   = false;
    private bLoad: boolean          = false;
    private bLoadDisp: boolean      = false;
    private bLoadInits: boolean     = false;
    private bLoadTag: boolean       = false;
    private bLoadView: boolean      = false;



    constructor(
        protected srvcDisp: kwStDataVal,
        protected srvcInit: kwStDataVal,
        protected srvcView: kwStDataVal)
    {
        const log: kwLog = new kwLog(this.sClass, "constructor");
        //console.log(log.called());
    }


    private setArry(sVal: any)      { this.sArry = sVal; }
    private setDisp(disp: any)      { this.disp = disp; this.dispDisp();}
    private setFltr(sVal: any)      { this.sFltr = sVal; }
    private setId(sId: any)         { this.sId = sId; this.dispId();}
    private setInits(inits: any)    { this.inits = inits; this.dispInits();}
    private setLink(sVal: any)      { this.sLink = sVal; }
    private setStyl(sVal: any)      { this.sStyl = sVal; }
    private setTag(tag: any)        { this.tag = tag; this.dispTag();}
    private setText(sVal: any)      { this.sText = sVal; }

    protected getDisp()             {return this.disp;}
    protected getView()             {return this.view;}

    private getId()                 {return this.sId;}
    private getInits()              {return this.inits;}
    private getTag()                {return this.tag;}
    private getType()               {return this.type;}


    protected abstract handleEventPage($event: object): void;
    protected abstract initPage(): void;
    protected abstract loadPage(): void;
    protected abstract navigate(sLink: string);
    protected abstract onChanged($event): void;
    protected abstract parseInits(inits: any): void;
    protected abstract parseView(view: any): void;
    protected abstract publish($event: object);

// @formatter:on

    protected init(): void
    {
        const log: kwLog = new kwLog(this.sClass, "init");
        //console.log(log.called());

        if (this.bInit)
        {
            console.error(log.info("already initialized"));
            return;
        }

        if (kw.isNull(this.srvcDisp))
        {
            console.error(log.invalid("srvcDisp"));
            return;
        }

        if (kw.isNull(this.srvcInit))
        {
            console.error(log.invalid("srvcInit"));
            return;
        }

        if (kw.isNull(this.srvcView))
        {
            console.error(log.invalid("srvcView"));
            return;
        }

        if (!this.initPage())
        {
            const sMsg = "error initializing derived object";
            console.error(log.info(sMsg));
            return;
        }

        if (!this.load())
        {
            const sMsg = "error loading";
            console.error(log.info(sMsg));
            return;
        }

        this.bInit = true;
    }


    protected load(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "load");
        //console.log(log.called());

        if (this.bLoad)
        {
            return true
        }

        if (!this.bLoadDisp)
        {
            if (!this.loadDisp())
            {
                console.error(log.failLoad("disp"));
                return false;
            }
        }

        if (!this.bLoadView)
        {
            if(!this.loadView())
            {
                console.error(log.failLoad("view"));
                return false
            }
        }

        if (!this.bLoadInits)
        {
            if(!this.loadInits())
            {
                console.error(log.failLoad("inits"));
                return false;
            }
        }

        if (!this.bCreateAttrs)
        {
            if(!this.createAttrs())
            {
                console.error(log.errCreate("attrs"));
                return false;
            }
        }

        if(!this.loadAttrs())
        {
            console.error(log.errLoad("attrs"));
            return false;
        }

        if (!this.loadPage())
        {
            console.error(log.failLoad("derivation"));
            return false
        }

        this.bLoad = true;

        return true;

    }

    private createAttrs(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "createAttrs");
        //console.log(log.called());

        if (this.createAttrs)
        {
            return true;
        }

        this.createAttrs = true;


        const disp = this.getDisp();
        if (!kw.isValid(disp))
        {
            console.error(log.empty("disp"));
            return false;
        }
        console.info(log.isObj("disp"), disp, "]");

        const view = this.getView();
        if (!kw.isValid(view))
        {
            console.error(log.invalid("view"));
            return false;
        }
        console.info(log.isObj("view"), view, "]");

        const inits = this.getInits();
        if (!kw.isValid(inits))
        {
            console.error(log.invalid("inits"));
            return false;
        }
        console.info(log.isObj("inits"), inits, "]");


        const attrs = new kwAttrs(view, disp, inits);
        if (!attrs.init())
        {
            console.error(log.errCreate("attrs"));
            return false;
        }
        console.info(log.isObj("attrs"), attrs, "]");

        this.attrs = attrs;

        return true;
    }

    private storeInits(inits: any)
    {
        const log: kwLog = new kwLog(this.sClass, "storeInits");
        //console.log(log.called());

        if (!kw.isValid(inits))
        {
            console.error(log.invalid("inits"));
            return;
        }
        console.info(log.is("inits", inits));

        this.setInits(inits);

        this.parseInits(inits);
    }

    private storeView(view: any)
    {
        const log: kwLog = new kwLog(this.sClass, "storeView");
        console.log(log.called());

        if (!kw.isValid(view))
        {
            console.error(log.invalid("view"));
            return;
        }
        console.info(log.isObj("view"), view, "]");

        this.setView(view);

        this.loadTag();

        this.parseView(view);
    }



    protected handleEvent($event)
    {
        const log: kwLog = new kwLog(this.sClass, "handleEvent");
        //console.log(log.called());

        if (kw.isNull($event))
        {
            console.error(log.invalid("$event"));
            return;
        }
        //console.info(log.is("$event", $event));

        const sLink: string = this.getLink();
        if (kw.isString(sLink))
        {
            const sMsg = "navigating to [" + sLink + "]";
            console.error(log.info("sMsg"));

            this.navigate(sLink);
            return;
        }

        this.handleEventPage($event);
    }



    private loadDisp(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadDisp");
        //console.log(log.called());

        if (this.bLoadDisp)
        {
            return true;
        }

        let disp = <kwDisp> this.srvcDisp.get();
        if (kw.isNull(disp))
        {
            console.error(log.invalid("disp"));
            return false;
        }
        //console.info(log.is("disp", disp));

        this.setDisp(disp);

        this.bDispAll = disp.dispAll();
        this.bDispInits = disp.dispInits();
        this.bDispTag = disp.dispTag();
        this.bDispView = disp.dispView();

        return true;
    }

    private loadAttrs(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadDisp");
        //console.log(log.called());

        let inits = this.getInits();
        if (!kw.isValid(inits))
        {
            console.error(log.invalid("inits"));
            return false;
        }
        console.info(log.is("inits", inits));

        let attrs = this.getAttrs();
        if (!kw.isValid(attrs))
        {
            console.error(log.invalid("attrs"));
            return false;
        }
        console.info(log.is("attrs", attrs));

        this.attrs.load(inits);

        this.setArry(this.attrs.getArry());
        this.setFltr(this.attrs.getFltr());
        this.setLink(this.attrs.getLink());
        this.setStyl(this.attrs.getStyl());
        this.setText(this.attrs.getText());

        return true;
    }

    private loadId(): void
    {
        const log: kwLog = new kwLog(this.sClass, "loadId");
        //console.log(log.called());

        if (this.bLoadId)
        {
            return;
        }

        this.bLoadId = true;

        const type = this.getType();
        if (!kw.isValid(type))
        {
            console.error(log.invalid("type"));
            this.setId(null);
            return;
        }

        let sId = type.sId;
        if (!kw.isString(sId))
        {
            console.info(log.empty("sId"));
            this.setId(null);
            return;
        }
        console.info(log.is("sId", sId));

        this.setId(sId);
    }

    private loadInits(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadInits");
        //console.log(log.called());

        if (this.bLoadInits)
        {
            return true;
        }

        this.bLoadInits = true;

        if (kw.isNull(this.srvcInit))
        {
            console.error(log.invalid("srvcInit"));
            return false;
        }

        const inits = this.srvcInit.get();
        if (kw.isNull(inits))
        {
            //console.info(log.empty("inits"));
            return true;
        }
        console.info(log.isObj("initsIn"), inits, "]");

        this.storeInits(inits);

        return true;
    }

    private loadTag()
    {
        const log: kwLog = new kwLog(this.sClass, "loadTag");
        //console.log(log.called());

        if (this.bLoadTag)
        {
            return;
        }

        const type = this.getType();
        if (!kw.isValid(type))
        {
            console.error(log.invalid("type"));
            this.setTag(null);
            this.bLoadTag = true;
            return;
        }

        const disp = this.getDisp();
        if (!kw.isValid(disp))
        {
            console.error(log.invalid("disp"));
            this.bLoadTag = true;
            this.setTag(null);
            return;
        }

        let tag = new kwTag(disp, type, this.sClass);
        if (!tag.init())
        {
            console.error(log.errCreate("tag"));
            this.bLoadTag = true;
            this.setTag(null);
            return;
        }
        //console.info(log.is("tag", tag));

        this.setTag(tag);
        this.bLoadTag = true;

        return;
    }

    private loadView(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadView");
        //console.log(log.called());

        if (this.bLoadView)
        {
            return;
        }

        this.bLoadView = true;

        if (kw.isNull(this.srvcView))
        {
            console.error(log.invalid("srvcView"));
             return false;
        }

        let view = this.srvcView.get();
        if (!kw.isValid(view))
        {
            console.error(log.invalid("view"));
            return false;
        }
        console.info(log.isObj("view"), view , "]");

        this.storeView(view);

        return true;
    }


    private dispDisp()
    {
        const log: kwLog = new kwLog(this.sClass, "dispDisp");
        if (!this.bDispDisp) {return;}
        const val = this.getDisp();
        if (kw.isValid(val)) {console.info(log.isObj("disp"), val, "]");}
    }

    private dispId()
    {
        const log: kwLog = new kwLog(this.sClass, "dispId");
        if (!this.bDispId) {return;}
        const val = this.getId();
        if (kw.isValid(val)) {console.info(log.is("sId", val));}
    }

    private dispInits()
    {
        const log: kwLog = new kwLog(this.sClass, "dispInits");
        if (!this.bDispInits) {return;}
        const val = this.getInits();
        if (kw.isValid(val)) {console.info(log.is("inits", val));}
    }

    private dispTag()
    {
        const log: kwLog = new kwLog(this.sClass, "dispTag");
        if (!this.bDispTag) {return;}
        const tag = this.getTag();
        if (!kw.isValid(tag)){console.error(log.invalid("tag")); return}
        this.sComp = tag.getTag();
        if (!kw.isString(this.sComp)){console.error(log.invalid("sComp"));}
    }

    private dispView()
    {
        const log: kwLog = new kwLog(this.sClass, "dispView");
        if (!this.bDispView) {return;}
        const val = this.getView();
        if (kw.isValid(val)) {console.info(log.isObj("view"), val, "]");}
    }



}
