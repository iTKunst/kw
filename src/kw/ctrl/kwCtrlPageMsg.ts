/**********************************************************************
 *
 * kw/ctrl/kwCtrlPage.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off

import {kw}                         from "@kw/kw"
import {kwAttrs}                    from "@kwClass/attrs/kwAttrs"
import {kwLog}                      from "@kw/kwLog"
import {kwCtrlPage}                 from "./kwCtrlPage";
import {kwStDataList}               from "@kwState/kwStDataList"
import {kwStDataVal}                from "@kwState/kwStDataVal"
import {kwtCtrlPageMsg}             from "@kwType/kwtCtrlPageMsg";


export abstract class kwCtrlPageMsg extends kwCtrlPage
{

    // available to bind to - depending on view settings
    public data: any;
    public local: any;

    // stored via http
    private master: any;

    private bLoadData: boolean      = false;
    private bLoadFltrData: boolean  = false;
    private bLoadFltrSt: boolean    = false;
    private bLoadFltrView: boolean  = false;
    private bLoadMaster: boolean    = false;
    private bLoadPage: boolean      = false;
    private bLoadPageDisp: boolean  = false;

    private bDispLocal: boolean     = false;
    private bDispMaster: boolean    = false;

    private fltrView: any;
    private fltrSt: any;
    private dataFltr: any;


    constructor(
        srvcDisp: kwStDataVal,
        srvcInit: kwStDataVal,
        srvcView: kwStDataVal,
        protected srvcData: kwStDataList)
    {
        super(srvcDisp, srvcInit, srvcView);

        const log: kwLog = new kwLog(this.sClass, "constructor");
        //console.log(log.called());
    }


    protected getData(): any {return this.data;}
    protected getDataFltr(): any {return this.dataFltr;}

    private getLocal(): any {return this.local;}
    private getMaster(): any {return this.master;}

    private setData(data: any): void {this.data = data; this.dispData();}
    private setDataFltr(data: any): void {this.dataFltr = data; this.dispDataFltr();}
    private setMaster(data: any): void {this.master = data; this.dispMaster();}

    protected abstract loadPageData(): void;
    protected abstract parseData(data: any): void;


//@formatter:on


    protected initPage(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "initPage");
        //console.log(log.called());

        if (kw.isNull(this.srvcData))
        {
            console.error(log.invalid("srvcData"));
            return false;
        }

        return true;
    }

    protected storeMaster(data): void
    {
        const log: kwLog = new kwLog(this.sClass, "storeMaster");
        //console.log(log.called());

        console.info(log.isObj("data"), data, "]");

        this.setMaster(data);

        this.setData(data);

        return;
    }

    protected loadPage(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadPage");
        //console.log(log.called());

        if (this.bLoadPage)
        {
            return;
        }

        if (!this.bLoadData)
        {
            if (!this.loadData())
            {
                console.error(log.errLoad("data"));
            }
        }

        this.bLoadPage = true;

        return true;
    }

    private loadFltrData(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadFltrData");
        //console.log(log.called());

        if (this.bLoadFltrData)
        {
            return true;
        }

        this.bLoadFltrData = true;

        const data = this.getDataFltr();
        if (kw.isNull(data))
        {
            console.error(log.invalid("data"));
            return false;
        }
        console.info(log.isObj("data"), data, "]");

        const sId = this.getFltrIdD();
        if (!kw.isString(sId))
        {
            console.error(log.invalid("sFltrIdD"));
            this.bLoadFltrData = true;
            return;
        }
        console.info(log.is("sFltrIdD", sId));

        const fltr = this.extractItem(data, sId);
        if (!kw.isValid(fltr))
        {
            console.error(log.failLoad("fltr"));
            this.bLoadFltrData = true;
            return;
        }
        console.info(log.is("fltr", fltr));

        this.storeFltr(fltr);

        this.bLoadFltrData = true;

        return true;
    }
    private loadFltrView(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadFltrView");
        //console.log(log.called());

        if (this.bLoadFltrView)
        {
            return true;
        }

        const view = this.getView();
        if (kw.isNull(view))
        {
            console.error(log.invalid("view"));
            this.bLoadFltrView = true;
            return false;
        }
        //console.info(log.isObj("view"), view, "]");

        const type: kwtCtrlPageMsg = <kwtCtrlPageMsg>view;

        this.bLoadFltrView = true;

        let fltr = type.fltr;
        if (!kw.isValid(fltr))
        {
            //console.info(log.is("fltr", fltr));
            return false;
        }

        this.setFltr(fltr);

        return true;
    }

    private loadPageDisp(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadPageDisp");
        //console.log(log.called());

        if (this.bLoadPageDisp)
        {
            return;
        }

        const disp = this.getDisp();
        if (kw.isNull(disp))
        {
            console.error(log.invalid("disp"));
            return false;
        }
        //console.info(log.is("disp", disp));

        this.bDispData = disp.dispData();
        this.bDispDataFltr = disp.dispDataFltr();

        this.bLoadPageDisp = true;

        return true;
    }

    protected handleEventPage($event)
    {
        const log: kwLog = new kwLog(this.sClass, "handleEventPage");
        //console.log(log.called());

        if (kw.isNull($event))
        {
            console.error(log.invalid("$event"));
            return;
        }
        //console.info(log.is("$event", $event));

        const sId: string = this.getLinkIdD();

        let event;

        if (kw.isValid(sId))
        {
            //console.info(log.is("sDataId", sId));
            event = {
                id: sId,
                event: $event
            };
        }
        else
        {
            event = $event
        }

        //console.info(log.is("event", event));

        this.publish(event);
    }
    protected storeFltr(fltr: any): void
    {
        const log: kwLog = new kwLog(this.sClass, "storeFltr");
        //console.log(log.called());

        //console.info(log.is("fltr", fltr));

        this.setFltr(fltr);

        this.loadPageData();
    }


//@formatter:off



    private dispData()
    {
        const log: kwLog = new kwLog(this.sClass, "dispData");
        if (!this.bDispData){return;}
        const val = this.getData();
        if (kw.isValid(val)){console.info(log.isObj("data"), val, "]");}
    }

    private dispDataFltr()
    {
        const log: kwLog = new kwLog(this.sClass, "dispDataFltr");
        if (!this.bDispDataFltr){return;}
        const val = this.getDataFltr();
        if (kw.isValid(val)) {console.info(log.isObj("dataFltr"), val, "]");}
    }

    private dispMaster()
    {
        const log: kwLog = new kwLog(this.sClass, "dispMaster");
        if (!this.bDispMaster) { return; }
        const val = this.getMaster();
        if (kw.isValid(val)) {console.info(log.isObj("master"), val, "]");}
    }

//@formatter:on


}
