/**********************************************************************
 *
 * kw/ctrl/kwCtrlComp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off

import {kw} from "@kw/kw";
import {kwDisp} from "@kwClass/disp/kwDisp";
import {kwLog} from "@kw/kwLog";
import {kwStDataVal} from "@kwState/kwStDataVal";
import {kwStore} from "@kwClass/store/kwStore";
import {kwTag} from "@kwClass/tag/kwTag";
import {kwtCtrlComp} from "@kwType/kwtCtrlComp";

const sTAG_STYLE: string            = "sStyle";
const sTAG_STYLE_DEFAULT: string    = "default";
const sTAG_TYPE: string             = "type";



export abstract class kwCtrlComp
{
    protected sClass: string = this.constructor.name;


    //values that are available for data binding.
    public data: any;
    public fltr: any;
    public inits: any;
    public view: any;

    public sComp: string;
    public sId: string;
    public sLink: string;
    public sStyle: string;
    public sText: string;


    // values from parent
    private kwData: any;
    private kwFltr: any;
    private kwInits: any;
    private kwView: any;

    // values used for local component
    private lclData: any;
    private lclInits: any;

    private disp: kwDisp;
    private tag: kwTag;
    private type: kwtCtrlComp;

    private sDataId: string;
    private sFltrId: string;
    private sInitId: string;
    private sLinkId: string;
    private sLinkTmpl: string;

    private bDispArry: boolean = false;
    private bDispArryId: boolean = false;
    private bDispArryIdD: boolean = false;
    private bDispId: boolean = false;
    private bDispData: boolean = false;
    private bDispFltr: boolean = false;
    private bDispFltrId: boolean = false;
    private bDispFltrIdD: boolean = false;
    private bDispInits: boolean = false;
    private bDispLink: boolean = false;
    private bDispLinkId: boolean = false;
    private bDispLinkIdD: boolean = false;
    private bDispLinkTmpl: boolean = false;
    private bDispStyl: boolean = false;
    private bDispStylIde: boolean = false;
    private bDispStylIdD: boolean = false;
    private bDispTag: boolean =false;
    private bDispText: boolean =false;
    private bDispTextId: boolean =false;
    private bDispTextIdD: boolean =false;
    private bDispView: boolean = false;

    private bInit: boolean = false;
    private bCheck: boolean = false;
    private bLoadArryId: boolean = false;
    private bLoadArryIdD: boolean = false;
    private bLoadDisp: boolean = false;
    private bLoadFltr: boolean = false;
    private bLoadFltrId: boolean = false;
    private bLoadFltrIdD: boolean = false;
    private bLoadInits: boolean = false;
    private bLoadLink: boolean =false;
    private bLoadLinkId: boolean = false;
    private bLoadLinkIdD: boolean = false;
    private bLoadLinkTmpl: boolean =false;
    private bLoadText: boolean =false;
    private bLoadTextId: boolean =false;
    private bLoadTextIdD: boolean =false;
    private bLoadStyl: boolean =false;
    private bLoadtylId: boolean =false;
    private bLoadtylIdD: boolean =false;
    private bLoadView: boolean = false;


    constructor(
        protected sTag: string,
        private srvcDisp: kwStDataVal)
    {
        //console.log(this.sClass, "::constructor() called");
    }

    protected abstract getDataIn(): any;
    protected abstract initCmp(): void;
    protected abstract loadData(): void;
    protected abstract navigate(sLink: string);
    protected abstract parseData(data: any): void;
    protected abstract parseInits(inits: any): void;
    protected abstract parseView(view: any): void;
    protected abstract publish($event: object);









    protected getFltr(): any          {return this.fltr;}

    private getArry(): any          {return this.arry;}
    private getArryId(): any       {return this.sArryIdD;}
    private getArryIdD(): any       {return this.sArryIdD;}
    private getData(): any          {return this.data;}
    private getDisp(): any          {return this.disp;}
    private getId(): any          {return this.sId;}
    private getInits(): any       {return this.inits;}
    private getDataFltr(): any      {return this.dataFltr;}
    private getFltrIdD(): any       {return this.sFltrIdD;}
    private getLinkIdD(): string    {return this.sLinkIdD;}
    private getLink(): string     {return this.sLink;}
    private getLinkId(): string   {return this.sLinkId;}
    private getLinkIdD(): string   {return this.sLinkId;}
    private getLinkTmpl(): string {return this.sLinkTmpl;}
    private getLocal(): any         {return this.local;}
    private getMaster(): any        {return this.master;}
    private getStyl(): string     {return this.sStyle;}
    private getStylId(): string   {return this.sStylId;}
    private getStylIdD(): string   {return this.sStylId;}
    private getTag(): any         {return this.tag;}
    private getText(): any        {return this.sText;}
    private getTextId(): any      {return this.sTextId;}
    private getTextIdD(): any      {return this.sTextId;}
    private getType(): any        {return this.type;}
    private getView(): any       {return this.view;}



    private setDisp(disp: any): void        {this.disp = disp; this.dispDisp();}
    private setId(sId: any): void           {this.sId = sId; this.dispId();}
    private setInits(inits: any): void      {this.inits = inits; this.dispInits();}
    private setFltr(sFltr: string): void    {this.sFltr = sFltr; this.dispFltr();}
    private setFltrId(sId: string): void    {this.sFltrId = sId; this.dispFltrId();}
    private setFltrIdD(sId: string): void   {this.sFltrIdD = sId; this.dispFltrIdD();}
    private setLink(sLink: string): void    {this.sLink = sLink; this.dispLink();}
    private setLinkId(sId: string): void    {this.sLinkId = sId; this.dispLinkId();}
    private setLinkIdD(sId: string): void   {this.sLinkIdD = sId; this.dispLinkIdD();}
    private setStyl(sStyl: any): void       {this.sStyle = sStyl; this.dispStyl();}
    private setStylId(sId: string): void    {this.sStylId = sId; this.dispStylId();}
    private setStylIdD(sId: string): void   {this.sStylIdD = sId; this.dispStylIdD();}
    private setTag(tag: any): void          {this.tag = tag; this.dispTag();}
    private setText(sText: any): void       {this.sText = sText; this.dispText();}
    private setTextId(sId: string): void    {this.sTextId = sId; this.dispTextId();}
    private setTextIdD(sId: string): void   {this.sTextIdD = sId; this.dispTextIdD();}
    private setType(type: any): void        {this.type = type;}
    private setView(view: any): void        {this.view = view; this.dispView();}



// @formatter:on


    protected init()
    {
        //console.log(this.sClass, "::init() called.");

        if (this.bInit)
        {
            //console.info(this.sClass, "::init() component ready.");
            this.updateData();
            return;
        }

        if (!this.bCheck)
        {
            if (!this.check())
            {
                console.error(this.sClass, "::init() check failed");
                return;
            }
        }

        if (!this.bLoadDisp)
        {
            this.loadDisp();
        }

        this.initCmp();


        if (kw.isNull(this.kwView))
        {
            //console.log(this.sClass, "::init() kwView not received.");
            return;
        }
        //console.info(this.sClass, "::init() kwView is [", this.kwView, "]");


        if (kw.isNull(this.kwInits))
        {
            //console.log(this.sClass, "::init() kwInits not received.");
            return;
        }
        //console.info(this.sClass, "::init() kwInits is [", this.kwInits, "]");

        if (!this.bLoadView)
        {
            if(!this.loadView())
            {
                console.error(this.sClass, "::init() error loading view.")
                return false;
            }
        }

        if (!this.bLoadInits)
        {
            if(!this.loadInits())
            {
                console.error(this.sClass, "::init() error loading inits.")
                return false;
            }
        }

        if (!this.bLoadFltr)
        {
            if(!this.loadFltr())
            {
                console.error(this.sClass, "::init() error loading fltr.")
                return false;
            }
        }

        this.bInit = true;

        this.kwData = this.getDataIn();
        if (kw.isValid(this.kwData))
        {
            this.updateData()
        }
        else
        {
            this.loadData();
        }

    }

    protected check(): boolean
    {
        //console.log(this.sClass, "::init() called.");

        if (this.bCheck)
        {
            console.error(this.sClass, "::check() already called");
            return true;
        }

        if ( !kw.isString(this.sTag)
            && this.sTag.length === 0  )
        {
            console.error(this.sClass, "::init() sTag is invalid.");
            return false;
        }

        if (kw.isNull(this.srvcDisp))
        {
            console.error(this.sClass, "::init() srvcDisp is invalid.");
            return false;
        }

        this.bCheck = true;

        return true;
    }

    protected storeData(data: any)
    {
        this.kwData = data;
        this.init();
    }

    protected storeFltr(fltr: any)
    {
        this.kwFltr = fltr;
        this.init();
    }

    protected storeInit(init: any)
    {
        this.kwInits = init;
        this.init();
    }

    protected storeView(view: any)
    {
        this.kwView = view;
        this.init();
    }

    protected getData(): any
    {
        return this.lclData;
    }

    protected getDataId(): string
    {
        return this.sDataId;
    }

    protected getDisp(): any
    {
        return this.disp;
    }

    protected getFltr(): any
    {
        return this.fltr;
    }

    protected getId(): any
    {
        return this.sId;
    }

    protected getInitId(): string
    {
        return this.sInitId;
    }

    protected getInits(): any
    {
        return this.inits;
    }

    protected getLink(): string
    {
        return this.sLink;
    }

    protected getLinkId(): string
    {
        return this.sLinkId;
    }

    protected getLinkTmpl(): string
    {
        return this.sLinkTmpl;
    }

    protected getStyle(): string
    {
        return this.sStyle;
    }

    protected getTag(): any
    {
        return this.tag;
    }

    protected getText(): any
    {
        return this.sText;
    }

    protected getView(): any
    {
        return this.view;
    }

    private setData(data: any): void
    {
        //console.log(this.sClass, "::setData() called.");
        //console.info(this.sClass, "::setData() data is [", data, "]");
        this.lclData = data;

        if (kw.isArray(data))
        {
            // it needs to go to children
            this.data = data
        }
        else
        {
            this.data = this.kwData;
        }

        this.dispData();
        this.parseData(data);
        this.loadLink();
        this.loadText();
    }

    private setDataId(sId: string): void
    {
        //console.log(this.sClass, "::setDataId() called.");
        //console.info(this.sClass, "::setDataId() sId is [", sId, "]");
        this.sDataId = sId;

        this.dispDataId();
    }

    private setDisp(disp: any): void
    {
        //console.log(this.sClass, "::setDisp() called.");
        //console.info(this.sClass, "::setDisp() disp is [", disp, "]");
        this.disp = disp;
    }

    protected setFltr(fltr: any): void
    {
        //console.log(this.sClass, "::setFltr() called.");
        //console.info(this.sClass, "::setFltr() fltr is [", fltr, "]");
        this.fltr = fltr;
        this.dispFltr();
    }

    private setInitId(sId: string): void
    {
        //console.log(this.sClass, "::setInitId() called.");
        //console.info(this.sClass, "::setInitId() sId is [", sId, "]");
        this.sInitId = sId;

        this.dispInitId();
    }

    private setInits(inits: any): void
    {
        //console.log(this.sClass, "::setInits() called.");
        //console.info(this.sClass, "::setInits() inits is [", inits, "]");
        this.inits = inits;
        this.dispInits();
        this.parseInits(inits);
    }

    private setLink(sLink: string): void
    {
        //console.log(this.sClass, "::setInits() called.");
        //console.info(this.sClass, "::setInits() link is [", link, "]");
        this.sLink = sLink;
        this.dispLink();
    }

    private setLinkId(sId: string): void
    {
        //console.log(this.sClass, "::setLinkId() called.");
        //console.info(this.sClass, "::setLinkId() sId is [", sId, "]");
        this.sLinkId = sId;

        this.dispLinkId();
    }

    private setLinkTmpl(sTmpl: string): void
    {
        //console.log(this.sClass, "::setLinkTmpl() called.");
        //console.info(this.sClass, "::setLinkTmpl() sTmpl is [", sTmpl, "]");
        this.sLinkTmpl = sTmpl;

        this.dispLinkTmpl();
    }

    private setTag(tag: any): void
    {
        //console.log(this.sClass, "::setTag() called.");
        //console.info(this.sClass, "::setTag() tag is [", tag, "]");
        this.tag = tag;
    }

    private setText(sText: string): void
    {
        //console.log(this.sClass, "::setView() called.");
        //console.info(this.sClass, "::setView() view is [", view, "]");
        this.sText = sText;
        this.dispText();
    }

    private setView(view: any): void
    {
        //console.log(this.sClass, "::setView() called.");
        //console.info(this.sClass, "::setView() view is [", view, "]");
        this.view = view;
        this.dispView();
        this.dispTag();
        this.parseView(view);
    }


    private dispData()
    {
        //console.log(this.sClass, "::dispData() called.");

        if (!this.bDispData)
        {
            return;
        }

        const data = this.getData();
        if (kw.isNull(data))
        {
            //console.info(this.sClass, "::dispData() data empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispData() data is [", data, "]");}
    }

    private dispDataId()
    {
        //console.log(this.sClass, "::dispDataId() called.");

        if (!this.bDispDataId)
        {
            return;
        }

        const sDataId: string = this.getDataId();
        if (!kw.isString(sDataId))
        {
            //console.info(this.sClass, "::dispDataId() sDataId empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispDataId() sDataId is [", sDataId, "]");}
    }

    private dispFltr()
    {
        //console.log(this.sClass, "::dispFltr() called.");

        if (!this.bDispFltr)
        {
            return;
        }

        const fltr = this.getFltr();
        if (kw.isNull(fltr))
        {
            //console.info(this.sClass, "::dispFltr() fltr empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispFltr() fltr is [", fltr, "]");}
    }

    private dispId()
    {
        //console.log(this.sClass, "::dispId() called.");

        if (!this.bDispId)
        {
            return;
        }

        const sId = this.getId();
        if (!kw.isString(sId))
        {
            //console.info(this.sClass, "::dispId() sId empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispId() sId is [", sId, "]");}
    }

    private dispInitId()
    {
        //console.log(this.sClass, "::dispInitId() called.");

        if (!this.bDispInitId)
        {
            return;
        }

        const sInitId: string = this.getInitId();
        if (!kw.isString(sInitId))
        {
            //console.info(this.sClass, "::dispInitId() sInitId empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispInitId() sInitId is [", sInitId, "]");}
    }

    private dispInits()
    {
        //console.log(this.sClass, "::dispInits() called.");

        if (!this.bDispInits)
        {
            return;
        }

        const inits = this.getInits();
        if (kw.isNull(inits))
        {
            //console.info(this.sClass, "::dispInits() inits empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispInits() inits is [", inits, "]");}
    }

    private dispLink()
    {
        //console.log(this.sClass, "::dispLink() called.");

        if (!this.bDispLink)
        {
            return;
        }

        const sLink: string = this.getLink();
        if (!kw.isString(sLink))
        {
            //console.info(this.sClass, "::dispLink() sLink empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispLink() sLink is [", sLink, "]");}
    }

    private dispLinkId()
    {
        //console.log(this.sClass, "::dispLinkId() called.");

        if (!this.bDispLinkId)
        {
            return;
        }

        const sLinkId = this.getLinkId();
        if (!kw.isString(sLinkId))
        {
            //console.info(this.sClass, "::dispLinkId() sLinkId empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispLinkId() sLinkId is [", sLinkId, "]");}
    }


    private dispLinkTmpl()
    {
        //console.log(this.sClass, "::dispLinkTmpl() called.");

        if (!this.bDispLinkTmpl)
        {
            return;
        }

        const sLinkTmpl = this.getLinkTmpl();
        if (!kw.isString(sLinkTmpl))
        {
            //console.info(this.sClass, "::dispLinkTmpl() sLinkTmpl empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispLinkTmpl() sLinkTmpl is [", sLinkTmpl, "]");}
    }

    private dispStyle()
    {
        //console.log(this.sClass, "::dispStyle() called.");

        if (!this.bDispStyle)
        {
            return;
        }

        const sStyle = this.getStyle();
        if (!kw.isString(sStyle))
        {
            //console.info(this.sClass, "::dispStyle() sStyle empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispStyle() sStyle is [", sStyle, "]");}
    }

    private dispTag()
    {
        //console.log(this.sClass, "::dispTag() called.");

        if (!this.bDispTag)
        {
            return;
        }

        if (kw.isNull(this.view))
        {
            //console.info(this.sClass, "::dispTag() data and view empty - not displaying.");
            return;
        }

        //console.info(this.sClass, "::dispTag() tag is [", tag, "]");
        this.loadTag();
        if (kw.isNull(this.tag))
        {
            console.error(this.sClass, "::dispTag() error loading tag.");
            return;
        }
        //console.info(this.sClass, "::dispTag() tag is [", tag, "]");

        this.sComp = this.tag.getTag();
        if (!kw.isString(this.sComp))
        {
            console.error(this.sClass, "::dispTag() sComp is invalid.");
        }
        //console.info(this.sClass, "::dispTag() sComp is [", this.sComp, "]");
    }

    private dispText()
    {
        //console.log(this.sClass, "::dispText() called.");

        if (!this.bDispText)
        {
            return;
        }

        const sText = this.getText();
        if (!kw.isString(sText))
        {
            //console.info(this.sClass, "::dispStyle() sStyle empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispText() sText is [", sText, "]");}
    }

    private dispView()
    {
        //console.log(this.sClass, "::dispView() called.");

        if (!this.bDispView)
        {
            //console.info(this.sClass, "::dispTag() bDispView is [", this.bDispView, "]");
            return;
        }

        const view = this.getView();
        if (kw.isNull(view))
        {
            //console.info(this.sClass, "::dispView() view empty - not displaying.");
            return;
        }
        else{console.info(this.sClass, "::dispView() view is [", view, "]"); }
    }

    protected handleEvent($event)
    {
        //console.log(this.sClass, "::handleEvent() called.");

        if (kw.isNull($event))
        {
            console.error(this.sClass, "::handleEvent() $event is invalid");
            return;
        }
        //console.info(this.sClass, "::handleEvent() $event is [", $event, "]");

        const sDataId: string = this.getDataId();

        const sLink: string = this.getLink();
        if (kw.isString(sLink))
        {
            //console.info(this.sClass, "::handleEvent() navigating to [", sLink, "]");

            this.navigate(sLink);
            return;
        }

        let event;

        if (kw.isValid(sDataId))
        {
            //console.info(this.sClass, "::handleEvent() sDataId is [", this.sDataId, "]");
            event={
                id: sDataId,
                event: $event
            };
        }
        else
        {
            event = $event
        }

        //console.info(this.sClass, "::handleEvent() publishing event [", event, "]");
        this.publish(event);

    }

    private loadDataId()
    {
        //console.log(this.sClass, "::loadDataId() called.");

        if (this.bLoadDataId)
        {
            //console.info(this.sClass, "::loadDataId() sDataId already created");
            return;
        }

        if (kw.isString(this.sDataId))
        {
            console.error(this.sClass, "::loadDataId() sDataId already created");
            this.bLoadDataId=true;
            return;
        }

        if (kw.isNull(this.type))
        {
            console.error(this.sClass, "::loadDataId() type is not provided ");
            return;
        }
        //console.info(this.sClass, "::loadDataId() type is [", this.type, "]");

        this.bLoadDataId=true;

        const sId = this.type.sDataId;
        if (!kw.isString(sId))
        {
            //console.info(this.sClass, "::loadDataId() sId is not provided ");
            return;
        }
        //console.info(this.sClass, "::loadDataId() sId is [", sId, "]");

        this.setDataId(sId)
    }

    private loadDisp(): boolean
    {
        //console.log(this.sClass, "::loadDisp() called.");

        if (this.bLoadDisp)
        {
            return;
        }

        if (kw.isValid(this.disp))
        {
            console.error(this.sClass, "::disp() already initialized");
            return false;
        }

        let disp = <kwDisp> this.srvcDisp.get();
        if (kw.isNull(disp))
        {
            console.error(this.sClass, "::init() disp is invalid ");
            return false;
        }
        //console.info(this.sClass, "::init() disp is [", disp, "]");

        this.setDisp(disp);

        this.bDispAll = disp.dispAll();
        this.bDispData = disp.dispData();
        this.bDispFltr = disp.dispFltr();
        this.bDispFltrId = disp.dispFltrId();
        this.bDispFltrIdD = disp.dispFltrIdD();
        this.bDispInits = disp.dispInits();
        this.bDispLink = disp.dispLink();
        this.bDispLinkId = disp.dispLinkId();
        this.bDispLinkIdD = disp.dispLinkIdD();
        this.bDispLinkTmpl = disp.dispLinkTmpl();
        this.bDispStyl = disp.dispStyl();
        this.bDispStylId = disp.dispStylId();
        this.bDispStylIdD = disp.dispStylIdD();
        this.bDispTag = disp.dispTag();
        this.bDispText = disp.dispText();
        this.bDispTextId = disp.dispTextId();
        this.bDispTextIdD = disp.dispTextIdD();
        this.bDispView = disp.dispView();

        this.bLoadDisp = true;

        return true;
    }

    private loadFltr(): boolean
    {
        //console.log(this.sClass, "::loadInits() called.");

        if (this.bLoadFltr)
        {
            //console.info(this.sClass, "::loadInits() fltr was already created ");
            return true;
        }

        this.bLoadFltr = true;

        this.setFltr(this.kwFltr);

        return true;
    }

    private loadId()
    {
        //console.log(this.sClass, "::loadId() called.");

        if (kw.isValid(this.sId))
        {
            //console.info(this.sClass, "::loadId() sId already created");
            return;
        }

        if (kw.isNull(this.type))
        {
            //console.info(this.sClass, "::loadId() view is not provided ");
            return;
        }

        let sId = this.type.sId;
        if (kw.isString(sId))
        {
            this.sId = sId;
        }

        this.dispId();
    }

    private loadInitId()
    {
        //console.log(this.sClass, "::loadInitId() called.");

        if (this.bLoadInitId)
        {
            console.error(this.sClass, "::loadInitId() sInitId was already created ");
            return true;
        }

        if (kw.isValid(this.sInitId))
        {
            //console.info(this.sClass, "::loadInitId() sInitId already created");
            this.bLoadInitId=true;
            return;
        }

        if (kw.isNull(this.type))
        {
            console.error(this.sClass, "::loadInitId() type is invalid. ");
            return;
        }

        this.bLoadInitId=true;

        let sId = this.type.sInitId;
        if (!kw.isString(sId))
        {
            //console.info(this.sClass, "::loadInitId() sId is not provided ");
            return;
        }
        //console.info(this.sClass, "::loadInitId() sId is [", sId, "]");

        this.setInitId(sId);
    }

    private loadInits(): boolean
    {
        //console.log(this.sClass, "::loadInits() called.");

        if (this.bLoadInits)
        {
            //console.info(this.sClass, "::loadInits() inits was already created ");
            return true;
        }

        let initsIn: any = this.kwInits;
        if (kw.isNull(initsIn))
        {
            console.error(this.sClass, "::loadInits() initsIn not provided ");
            return false;
        }
        //console.info(this.sClass, "::loadInits() initsIn is [", initsIn, "]");

        const sId = this.getInitId();
        if (!kw.isString(sId))
        {
            //console.info(this.sClass, "::loadInits() no sInitId provided");
            this.bLoadInits = true;
            this.setInits(initsIn);
            return true;
        }
        //console.info(this.sClass, "::loadInits() sId is [", sId, "]");


        this.bLoadInits = true;


        const initsNew = initsIn[sId];
        if (kw.isNull(initsNew))
        {
            console.error(this.sClass, "::loadInits() initsNew is invalid");
            this.setInits(initsIn);
            return true;
        }
        //console.info(this.sClass, "::loadInits() initsNew is [", initsNew, "]");

        this.setInits(initsNew);

        return true;
    }

    protected loadLink(): void
    {
        //console.log(this.sClass, "::loadLink() called.");

        if (this.bLoadLink)
        {
            //console.info(this.sClass, "::loadLink() already loaded");
            return;
        }

        if (kw.isNull(this.type))
        {
            console.error(this.sClass, "::loadLink() type is invalid");
            return;
        }
        //console.info(this.sClass, "::loadLink() type is [", this.type, "]");

        const sLink = this.type.sLink;
        if (kw.isString(sLink))
        {
            //console.info(this.sClass, "::loadLink() sLink is [", sLink, "]");
            this.setLink(sLink);
            this.bLoadLink = true;
            return;
        }
        //console.info(this.sClass, "::loadLink() sLink is not provided");

        const data = this.getData();
        if (kw.isNull(data))
        {
            //console.info(this.sClass, "::loadLink() data is invalid");
            return;
        }
        //console.info(this.sClass, "::loadInits() data is [", data, "]");

        const sId = this.getLinkId();
        if (!kw.isString(sId))
        {
            //console.info(this.sClass, "::createVal() sLinkId not provided - not using data");
            this.bLoadLink = true;
            return;
        }
        //console.info(this.sClass, "::createVal() sLinkId is [", sId, "]");

        const val = data[sId];
        if ( !kw.isString(val)
            &&   !kw.isNumber(val) )
        {
            console.error(this.sClass, "::createVal() val is invalid");
            return;
        }
        //console.info(this.sClass, "::createVal() val is [", val, "]");

        let sVal;

        const sTmpl = this.getLinkTmpl();
        if (!kw.isString(sTmpl))
        {
            //console.info(this.sClass, "::createVal() not using template");
            sVal = val;
        }
        else
        {
            //console.info(this.sClass, "::createVal() sTmpl is [", sTmpl, "]");
            sVal = sTmpl.replace("[0]", val);
        }

        if (!kw.isString(sVal))
        {
            console.error(this.sClass, "::createVal() error creating sVal.");
            return;
        }

        //console.info(this.sClass, "::createVal() sLink is [", sVal, "].");


        this.setLink(sVal);

    }

    private loadLinkId()
    {
        //console.log(this.sClass, "::loadLinkId() called.");

        if (this.bLoadLinkId)
        {
            //console.info(this.sClass, "::loadLinkId() sLinkId already created");
            return;

        }

        if (kw.isNull(this.type))
        {
            console.error(this.sClass, "::loadLinkId() type is invalid");
            return;
        }

        this.bLoadLinkId = true;

        let sId = this.type.sLinkId;
        if (!kw.isString(sId))
        {
            //console.info(this.sClass, "::loadLinkId() sLinkId not provided");
            return;
        }
        //console.info(this.sClass, "::loadLinkId() sLinkId is[", sLinkId, "]");

        this.setLinkId(sId);
    }

    private loadLinkTmpl()
    {
        //console.log(this.sClass, "::loadLinkTmpl() called.");

        if (this.bLoadLinkTmpl)
        {
            //console.info(this.sClass, "::loadLinkTmpl() sLinkTmpl already created");
            return;
        }

        if (kw.isValid(this.sLinkTmpl))
        {
            //console.info(this.sClass, "::loadLinkTmpl() sLinkTmpl already created");
            this.bLoadLinkTmpl=true;
            return;
        }

        if (kw.isNull(this.type))
        {
            console.error(this.sClass, "::loadLinkTmpl() type is invalid ");
            return;
        }

        this.bLoadLinkTmpl=true;

        let sTmpl = this.type.sLinkTmpl;
        if (!kw.isString(sTmpl))
        {
            //console.info(this.sClass, "::loadLinkTmpl() sTmpl is not provided.");
            return
        }
        //console.info(this.sClass, "::loadLinkTmpl() sTmpl is[", sTmpl, "]");
        this.setLinkTmpl(sTmpl);
    }

    private loadTag()
    {
        //console.log(this.sClass, "::loadTag() called.");

        if (kw.isValid(this.tag))
        {
            //console.info(this.sClass, "::loadTag() tag was already created ");
            return true;
        }

        if (kw.isNull(this.view))
        {
            this.view = {};
        }

        if (kw.isNull(this.srvcDisp))
        {
            console.error(this.sClass, "::loadTag() srvcDisp is not provided ");
            return;
        }

        let disp: kwDisp = this.srvcDisp.get();
        if (kw.isNull(disp))
        {
            console.error(this.sClass, "::loadTag() disp is invalid");
            return;
        }

        let tag = new kwTag(disp, this.view, this.sClass);
        if (!tag.init())
        {
            console.error(this.sClass, "::loadTag() error creating tag.");
            return;
        }
        //console.info(this.sClass, "::loadTag() tag is [", tag, "]");
        this.tag = tag;

        return;
    }

    private loadStyle()
    {
        //console.log(this.sClass, "::loadStyle() called.");

        if (kw.isValid(this.sStyle))
        {
            //console.info(this.sClass, "::loadTag() sStyle was already created ");
            return;
        }

        if (kw.isNull(this.type))
        {
            //console.info(this.sClass, "::loadStyle() view is not provided ");
            this.sStyle = sTAG_STYLE_DEFAULT;
        }
        else
        {
            let sStyle = this.type.sStyle;
            if (!kw.isString(sStyle))
            {
                //console.info(this.sClass, "::loadStyle() sClass is not provided");
                this.sStyle = sTAG_STYLE_DEFAULT;
            }
            else
            {
                this.sStyle = sStyle;
            }
        }

        this.dispStyle();

    }

    protected loadText(): void
    {
        const log = new kwLog(this.sClass, "loadText");
        //console.log(log.called());

        if (this.bLoadText)
        {
            return;
        }

        if (!kw.isValid(this.type))
        {
            console.error(log.invalid("type"));
            this.bLoadText=true;
            return;
        }

        const sId = this.getDataId();
        if (!kw.isString(sId)) {
            //console.info(log.invalid("sId"));

            const sText = this.type.sText;
            //console.info(log.is("sText", sText));

            this.setText(sText);
            this.bLoadText=true;
            return;
        }

        //console.info(log.is("sId", sId));

        const data = this.getData();
        //console.info(log.is("data", data));

        if (!kw.isString(data))
        {
            this.setText(null);
            return;
        }

        const sText = data;
        //console.info(log.is("sText", sText));

        this.setText(sText);
    }

    private loadView(): boolean
    {
        //console.log(this.sClass, "::loadView() called.");

        if (this.bLoadView)
        {
            return;
        }

        let viewIn: any = this.kwView;
        if (kw.isNull(viewIn))
        {
            console.error(this.sClass, "::loadView() viewIn not provided ");
            this.bLoadView = true;
            return false;
        }
        //console.info(this.sClass, "::loadView() viewIn is [", viewIn, "]");

        if (!kw.isString(this.sTag))
        {
            console.error(this.sClass, "::loadView() sTag is invalid");
            this.bLoadView = true;
            return false;
        }

        this.bLoadView = true;

        let view;

        if (this.sTag.length === 0)
        {
            //console.i(this.sClass, "::loadView() sTag is empty - using viewParent");
            view = viewIn;
        }
        else
        {
            //console.info(this.sClass, "::loadView() sTag is [", this.sTag, "]");
            view = viewIn[this.sTag];
        }

        if (kw.isNull(view))
        {
            console.error(this.sClass, "::loadView() no view available");
            this.bLoadView = true;
            return false;
        }
        //console.info(this.sClass, "::loadView() view is [", view, "]");

        this.type = <kwtCtrlComp>view;

        this.loadDataId();
        this.loadId();
        this.loadLink();
        this.loadInitId();
        this.loadLinkId();
        this.loadLinkTmpl();
        this.loadStyle();
        this.loadText();

        this.setView(view);

        return true;
    }

    protected extractItem(info: any, sKey: string): any
    {
        const log: kwLog = new kwLog(this.sClass, "extractItem");
        //console.log(log.called());


        if (!kw.isValid(info))
        {
            console.error(log.invalid(info));
        }
        //console.info(log.is("info", info));


        if (!kw.isString(sKey))
        {
            console.error(log.invalid(sKey));
        }
        //console.info(log.is("sKey", sKey));


        let data = new kwStore(info, sKey);
        if (!data.init())
        {
            console.error(log.errCreate("data"));
        }

        return data.get();
    }

}
