/**********************************************************************
 *
 * kw/ctrl/kwCtrlPage.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off

import { kw }                from "@kw/kw"
import { kwLog }             from "@kw/kwLog"
import { kwCtrlPage }        from "./kwCtrlPage";
import { kwStDataVal }       from "@kwState/kwStDataVal"


export abstract class kwCtrlPageSmpl extends kwCtrlPage
{

	constructor(
		srvcDisp: kwStDataVal,
		srvcInit: kwStDataVal,
		srvcView: kwStDataVal  )
	{
		super(srvcDisp, srvcInit, srvcView );

        const log: kwLog = new kwLog(this.sClass, "constructor");
        //console.log(log.called());
	}

// @formatter:on

    protected initPage(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "initPage");
        //console.log(log.called());

        return true;
    }

    protected checkPage(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "checkPage");
        //console.log(log.called());

        return true;
    }

    protected handleEventPage($event)
    {
        const log: kwLog = new kwLog(this.sClass, "handleEventPage");
        //console.log(log.called());

        if (kw.isNull($event))
        {
            console.error(log.invalid("$event"));
            return;
        }
        //console.info(log.is("$event", $event));

         this.publish(event);
    }

    protected loadDispPage(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadDispPage");
        //console.log(log.called());

        return true;
    }

    protected loadLinkPage(): void
    {
        const log: kwLog = new kwLog(this.sClass, "loadLinkPage");
        //console.log(log.called());
    }

    protected loadViewPage(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadViewPage");
        //console.log(log.called());

        return true;
    }

    protected loadPage(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadPage");
        //console.log(log.called());

        return true;
    }

}
