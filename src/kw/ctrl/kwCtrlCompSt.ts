/**********************************************************************
 *
 * kw/ctrl/kwCtrlComp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import { kw }                       from "@kw/kw"
import { kwCtrlComp }               from "./kwCtrlComp";
import { kwStData }                 from "@kwState/kwStData"
import { kwStDataVal }              from "@kwState/kwStDataVal"


export abstract class kwCtrlCompSt extends kwCtrlComp
{

	constructor(
		sTag: string,
		srvcDisp: kwStDataVal,
		protected srvcData: kwStData )
	{
		super(sTag, srvcDisp);
		//console.log(this.sClass, "::constructor() called");
	}

	protected abstract subscribe(): void;

// @formatter:on

	protected loadData(): boolean
	{
		//console.log(this.sClass, "::loadData() called.");

		this.subscribe();

		return true;
	}


}
