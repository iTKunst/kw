/**********************************************************************
 *
 * kw/ctrl/kwCtrlCompMsg.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
// @formatter:off
import { kw }                       from "@kw/kw"
import { kwCtrlComp }               from "./kwCtrlComp";
import { kwStDataVal }              from "@kwState/kwStDataVal"


export abstract class kwCtrlCompId extends kwCtrlComp
{

	constructor(
		sTag: string,
		srvcDisp: kwStDataVal   )
	{
		super(sTag, srvcDisp);
		//console.log(this.sClass, "::constructor() called");
	}


// @formatter:on

	protected loadData(): boolean
	{
		//console.log(this.sClass, "::loadData() called.");

		return true;
	}

}
