/**********************************************************************
 *
 * kw/class/api/kwtBS.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kwtAttr }              from "@kwClass/attr/kwtAttr";
import { kwtCred }              from "@kwClass/cred/kwtCred";
import { kwtDisp }              from "@kwClass/disp/kwtDisp";
import { kwtRoutes }            from "@kwClass/routes/kwtRoutes";
import { kwtSrvc }              from "@kwClass/srvc/kwtSrvc";
// @formatter:on


export class kwtBS
{
	apis: object;
	attrs: kwtAttr[];
    bAutoLogin: boolean;
    bTraceApp: boolean;
    bTraceRoute: boolean;
    bTraceState: boolean;
	credentials: kwtCred;
	display: kwtDisp;
	mdls: object;
	sMode: string;
	sRedirect: string;
 	routes: kwtRoutes;
	srvcs: kwtSrvc[];
}
