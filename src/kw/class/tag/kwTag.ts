/**********************************************************************
 *
 * kw/class/tag/kwTag.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {kw} from "@kw/kw";

import {kwDisp} from "@kwClass/disp/kwDisp";
import {kwTagSrvc} from "./kwTagSrvc";
import {kwTagType} from "./kwTagType";


const sTAG_ID: string = "sId";
const sTAG_STYLE: string = "sStyle";

const sTAG_TEMPLATE_CLASS: string = "class";
const sTAG_TEMPLATE_CLOSE: string = ">";
const sTAG_TEMPLATE_DATA_ID: string = "data";
const sTAG_TEMPLATE_EQUAL: string = "=";
const sTAG_TEMPLATE_ID: string = "id";
const sTAG_TEMPLATE_OPEN: string = "<";
const sTAG_TEMPLATE_STYLE_DEFAULT: string = "default";

export class kwTag
{
    private bDispId = false;
    private bDispStyle = false;
    private bDispTag = false;

    sId: string;
    sTag: string;
    sStyle: string;

    constructor(
        private disp: kwDisp,
        private view: object,
        private sClass: string)
    {
        //console.log("kwTag::constructor() called");
    };

// @formatter:on

    init(): boolean
    {
        //console.log("kwTag::init() is called.");

        if (kw.isNull(this.disp))
        {
            console.error("kwTag::init() disp is invalid.");
            return false;
        }
        //console.info("kwTag::init() disp is ", this.disp);


        if (kw.isNull(this.view))
        {
            console.error("kwTag::init() view is invalid.");
            return false;
        }
        //console.info("kwTag::init() view is ", this.view);


        if (!kw.isString(this.sClass))
        {
            console.error("kwTag::init() sClass is invalid.");
            return false;
        }
        //console.info("kwTag::init() sClass is [", this.sClass, "]");


        if (!this.retrieve())
        {
            console.error("kwTag::init() error retrieving values.");
            return false;
        }

        this.create();

        return true;
    }

    getTag(): string
    {
        return this.sTag;
    };

    create(): void
    {
        //console.info("kwTag::create() is called.");

        if (!this.bDispId
            && !this.bDispStyle
            && !this.bDispTag)
        {
            return;
        }

        let sTag = sTAG_TEMPLATE_OPEN;

        if (this.bDispTag)
        {
            sTag += this.sClass;
        }

        if (this.bDispStyle && kw.isString(this.sStyle))
        {
            sTag += " "
                + sTAG_TEMPLATE_CLASS
                + sTAG_TEMPLATE_EQUAL
                + this.sStyle;
        }

        if (this.bDispId && kw.isString(this.sId))
        {
            sTag += " "
                + sTAG_TEMPLATE_ID
                + sTAG_TEMPLATE_EQUAL
                + this.sId;
        }

        sTag += sTAG_TEMPLATE_CLOSE;
        //console.log("kwTag::create() sTag is [", sTag, "].");

        this.sTag = sTag;

    }

    retrieve(): boolean
    {
        //console.log("kwTag::retrieve() is called.");

        if (kw.isNull(this.disp))
        {
            console.error("kwTag::retrieve() disp is invalid.");
            return false;
        }
        //console.info("kwTag::retrieve() disp is ", this.disp);


        if (kw.isNull(this.view))
        {
            console.error("kwTag::retrieve() view is invalid.");
            return false;
        }
        //console.info("kwTag::retrieve() view is ", this.view);


        let bDispId = this.disp.dispId();
        if (!kw.isBoolean(bDispId))
        {
            console.error("kwTag::retrieve() bDispId is invalid.");
            return false;
        }
        //console.info("kwTag::retrieve() bDispId is [", bDispId, "]");
        this.bDispId = bDispId;


        let bDispStyle = this.disp.dispStyl();
        if (!kw.isBoolean(bDispStyle))
        {
            console.error("kwTag::retrieve() bDispStyle is invalstyle.");
            return false;
        }
        //console.info("kwTag::retrieve() bDispStyle is [", bDispStyle, "]");
        this.bDispStyle = bDispStyle;


        let bDispTag = this.disp.dispTag();
        if (!kw.isBoolean(bDispTag))
        {
            console.error("kwTag::retrieve() bDispTag is invalid.");
            return false;
        }
        //console.info("kwTag::retrieve() bDispTag is [", bDispTag, "]");
        this.bDispTag = bDispTag;

        const type: kwTagType = <kwTagType>this.view;


        let sStyle = type.sStyle;
        if (!kw.isString(sStyle))
        {
            sStyle = sTAG_TEMPLATE_STYLE_DEFAULT;
        }
        //console.info("kwTag::retrieve() sStyle is ", sStyle);
        this.sStyle = sStyle;


        let sId = type.sId;
        if (kw.isString(sId))
        {
            this.sId = sId;
            //console.info("kwTag::retrieve() sId is ", this.sId);
        }

        return true;
    }


    toString(): string
    {
        return this.constructor.name;
    };

    static is(obj: object): boolean
    {
        return kw.is(obj, kwTag);
    }
}

