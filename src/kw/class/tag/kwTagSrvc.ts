/**********************************************************************
 *
 * kw/class/tag/kwTagSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {kw }		            from "@kw/kw";

import {kwTagType }			    from "./kwTagType";
// @formatter:on


export class kwTagSrvc
{

	static isType(obj: object): boolean
	{
		return kw.is(obj, kwTagType)
	}

	static in(nVal: number): boolean
	{
		return false
	}

	static toEnum(sVal: string): number
	{
		return -1;
	};
}

