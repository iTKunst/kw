/**********************************************************************
 *
 * kw/class/tag/kwTagType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
// @formatter:on


export class kwTagType
{
	dataId: any;
	sClass: string;
	sId: string;
	sStyle: string;
}
