
/**********************************************************************
 *
 * kw/class/attr/kwAttrs.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off

import { kw }		        from "@kw/kw";
import { kwDisp }		    from "@kwClass/disp/kwDisp";
import { kwLog }		    from "@kw/kwLog";
import { kwStore }		    from "@kwClass/store/kwStore";
import { kwtAttr }          from "./kwtAttr";



export class kwAttr
{
    protected sClass: string = this.constructor.name;

	private _val: any;
    private _sId: string;
    private _sIdD: string;
    private _sName: string;

    private bInit: boolean            = false;
    private bLoadAttr: boolean        = false;
    private bLoadDisp: boolean        = false;
    private bLoadId: boolean          = false;
    private bLoadIdD: boolean         = false;
    private bLoadVal: boolean         = false;

    private _bDispAll: boolean         = false;
    private _bDispId: boolean          = false;
    private _bDispIdD: boolean         = false;
    private _bDispTmpl: boolean        = false;
    private _bDispVal: boolean         = false;

    private bDispAllAll: boolean        = false;

    private _sTag: string;


	constructor(
	    private attr: kwtAttr,
	    private disp: kwDisp,
        private inits: object,
	    private view: object,
        private sDefault?: string )
	{
        const log: kwLog = new kwLog(this.sClass, "constructor");
        console.log(log.called());
	}


	get sId(): string           { return this._sId; }
	get sIdD(): string          { return this._sIdD; }
	get sName(): string         { return this._sName; }
	get sTag(): string          { return this._sTag; }
	get val(): any              { return this._val; }

	get bDispAll(): boolean    { return this._bDispAll; }
	get bDispId(): boolean     { return this._bDispId; }
	get bDispIdD(): boolean    { return this._bDispIdD; }
	get bDispTmpl(): boolean   { return this._bDispTmpl; }
	get bDispVal(): boolean    { return this._bDispVal; }


//@formatter:on


	public init(): boolean
	{
        const log: kwLog = new kwLog(this.sClass, "init");
        console.log(log.called());

        if (this.bInit)
        {
            return;
        }

        this.bInit = true;


        const attr = this.attr;
        if (!kw.isValid(attr))
        {
            console.error(log.invalid("attr"));
            return false;
        }
        console.info(log.is("attr", attr));


        const disp = this.disp;
        if (!kw.isValid(this.disp))
        {
            console.error(log.invalid("disp"));
            return false;
        }
        console.info(log.is("disp", disp));


        const view = this.view;
        if (!kw.isValid(this.view))
        {
            console.error(log.invalid("view"));
            return false;
        }
        console.info(log.is("view", view));


        this.loadAttr();
        this.loadDisp();
        this.loadId();
        this.loadDisp();
        this.loadIdD();
        this.loadVal();

		return true;
	}

    public retrieve(data: object)
    {
        const log: kwLog = new kwLog(this.sClass, "retrieve");
        console.log(log.called());

        const sDefault = this.sDefault;
        console.info(log.is("sDefault", sDefault));

        const inits = this.inits;

        let val;

        const sIdD = this.sIdD;
        if (kw.isString(sIdD))
        {
            if (kw.isValid(data))
            {
                console.info(log.isObj("data"), data, "]");
                console.info(log.is("sIdD", sIdD));
                val = this.extractData(data, sIdD);
            }
        }
        else
        {
            const sId = this.sId;
            if (kw.isString(sId))
            {
                if (kw.isValid(inits))
                {
                    console.info(log.isObj("inits"), data, "]");
                    console.info(log.is("sId", sId));
                    val = this.extractData(inits, sId);
                }
            }
            else
            {
                val = this.val;
            }
        }

        if (!kw.isString(val))
        {
            val = this.sDefault;
        }

        this._val = val;
    }

    private loadAttr(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadAttr");
        console.log(log.called());

        if (this.bLoadAttr)
        {
            return true;
        }

        this.bLoadAttr = true;

        const attr = this.attr;
        if (!kw.isValid(attr))
        {
            console.error(log.invalid("attr"));
            return false;
        }
        console.info(log.is("attr", attr));


        const sName = attr.sName;
        if (!kw.isString(sName))
        {
            console.error(log.invalid("sName"));
            return false;
        }
        console.info(log.is("sName", sName));
        this._sName = sName;

        const bDispAll = attr.bDispAll;
        if (!kw.isBoolean(bDispAll))
        {
            console.error(log.invalid("bDispAll"));
            return false;
        }
        console.info(log.is("bDispAll", bDispAll));
        this._bDispAll = bDispAll;


        const bDispId = attr.bDispId;
        if (!kw.isBoolean(bDispId))
        {
            console.error(log.invalid("bDispId"));
            return false;
        }
        console.info(log.is("bDispId", bDispId));
        this._bDispId = bDispId;


        const bDispIdD = attr.bDispIdD;
        if (!kw.isBoolean(bDispIdD))
        {
            console.error(log.invalid("bDispIdD"));
            return false;
        }
        console.info(log.is("bDispIdD", bDispIdD));
        this._bDispIdD = bDispIdD;


        const bDispTmpl = attr.bDispTmpl;
        if (!kw.isBoolean(bDispTmpl))
        {
            console.error(log.invalid("bDispTmpl"));
            return false;
        }
        console.info(log.is("bDispTmpl", bDispTmpl));
        this._bDispTmpl = bDispTmpl;


        const bDispVal = attr.bDispVal;
        if (!kw.isBoolean(bDispVal))
        {
            console.error(log.invalid("bDispVal"));
            return false;
        }
        console.info(log.is("bDispVal", bDispVal));
        this._bDispVal = bDispVal;

        return true;
    }


    private loadDisp(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadDisp");
        console.log(log.called());

        if (this.bLoadDisp)
        {
            return true;
        }

        this.bLoadDisp = true;


        const disp = this.disp;
        if (!kw.isValid(disp))
        {
            console.error(log.invalid("disp"));
            return false;
        }
        console.info(log.is("disp", disp));


        const bDispAllAll = disp.bDispAll;
        if (!kw.isBoolean(bDispAllAll))
        {
            console.error(log.invalid("bDispAllAll"));
            return false;
        }
        console.info(log.is("bDispAllAll", bDispAllAll));
        this.bDispAllAll = bDispAllAll;

        return true;

    }


    private loadId(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadId");
        console.log(log.called());

        if (this.bLoadId)
        {
            return true;
        }

        this.bLoadId = true;

        const view = this.view;
        if (!kw.isValid(view))
        {
            console.error(log.invalid("view"));
            return false;
        }
        console.info(log.is("view", view));


        const sName = this.sName;
        if (!kw.isString(sName))
        {
            console.error(log.invalid("sName"));
            return false;
        }
        console.info(log.is("sName", sName));


        const sTag = "s" + sName + "Id";
        console.info(log.is("sTag", sTag));

        const sId = view[sTag];
        if (!kw.isValid(sId))
        {
            console.error(log.errLoad("sId"));
            return false;
        }
        console.info(log.is("sId", sId));
        this._sId = sId;

        return true;
    }


    private loadIdD(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadIdD");
        console.log(log.called());

        if (this.bLoadIdD)
        {
            return true;
        }

        this.bLoadIdD = true;

        const view = this.view;
        if (!kw.isValid(view))
        {
            console.error(log.invalid("view"));
            return false;
        }
        console.info(log.is("view", view));


        const sName = this.sName;
        if (!kw.isString(sName))
        {
            console.error(log.invalid("sName"));
            return false;
        }
        console.info(log.is("sName", sName));


        const sTagIdD = sName + "IdD";
        console.info(log.is("sTagIdD", sTagIdD));

        const sIdD = view[sTagIdD];
        if (!kw.isString(sIdD))
        {
            console.error(log.errLoad("sIdD"));
            return false;
        }
        console.info(log.is("sIdD", sIdD));
        this._sIdD = sIdD;

        return true;
    }

    private loadVal(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "loadVal");
        console.log(log.called());

        if (this.bLoadVal)
        {
            return true;
        }

        this.bLoadVal = true;

        const view = this.view;
        if (!kw.isValid(view))
        {
            console.error(log.invalid("view"));
            return false;
        }
        console.info(log.is("view", view));


        const sName = this.sName;
        if (!kw.isString(sName))
        {
            console.error(log.invalid("sName"));
            return false;
        }
        console.info(log.is("sName", sName));


        const sTag = "s" + sName;
        console.info(log.is("sTag", sTag));

        const val = view[sTag];
        if (!kw.isValid(val))
        {
            console.error(log.errLoad("val"));
            return false;
        }
        console.info(log.is("val", val));
        this._val = val;

        return true;
    }

    protected extractData(info: any, sKey: string): any
    {
        const log: kwLog = new kwLog(this.sClass, "extractData");
        //console.log(log.called());


        if (!kw.isValid(info))
        {
            console.error(log.invalid(info));
        }
        //console.info(log.is("info", info));


        if (!kw.isString(sKey))
        {
            console.error(log.invalid(sKey));
        }
        //console.info(log.is("sKey", sKey));


        let data = new kwStore(info, sKey);
        if (!data.init())
        {
            console.error(log.errCreate("data"));
        }

        return data.get();
    }


    private dispId()
    {
        const log: kwLog = new kwLog(this.sClass, "dispId");
        if ( !this.bDispAll && !this.bDispAllAll && !this.bdispId ) {return;}
        const val = this.sId;
        if (kw.isValid(val)) {console.info(log.is("sId", val));}
    }

    private dispIdD()
    {
        const log: kwLog = new kwLog(this.sClass, "dispId");
        if ( !this.bDispAll && !this.bDispAllAll && !this.bdispIdD ) {return;}
        const val = this.sId;
        if (kw.isValid(val)) {console.info(log.is("sId", val));}
    }

    private dispTmpl()
    {
        const log: kwLog = new kwLog(this.sClass, "dispTmpl");
        if ( !this.bDispAll && !this.bDispAllAll && !this.bdispTmpl ) {return;}
        const tmpl = this.tmpl;
        if (kw.isTmplid(tmpl)) {console.info(log.isObj("tmpl"), tmpl, "]");}
    }

    private dispVal()
    {
        const log: kwLog = new kwLog(this.sClass, "dispVal");
        if ( !this.bDispAll && !this.bDispAllAll && !this.bdispVal) {return;}
        const val = this.val;
        if (kw.isValid(val)) {console.info(log.isObj("val"), val, "]");}
    }


}
