
/**********************************************************************
 *
 * kw/class/attr/kwAttrs.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off

import { kw }		        from "@kw/kw";
import { kwAttr }           from "@kwClass/attr/kwAttr";
import { kwDisp }		    from "@kwClass/disp/kwDisp";
import { kwLog }		    from "@kw/kwLog";
import { kwView}		    from "@kwClass/view/kwView";

const sTAG_STYLE: string            = "sStyle";
const sTAG_STYLE_DEFAULT: string    = "default";
const sTAG_TYPE: string             = "type";



export class kwAttrs
{
    protected sClass: string = this.constructor.name;

    private items: kwAttr[];

    private bInit: boolean          = false;
    private bCreate: boolean    = false;


	constructor(
	    private _attrs: object,
	    private _disp: kwDisp,
	    private _view: kwView,
        private _inits: object  )
	{
        const log: kwLog = new kwLog(this.sClass, "constructor");
        console.log(log.called());
	}


    private get attrs() { return this._attrs; }
    private get disp() { return this._disp; }
    private get inits() { return this._inits; }
    private get view() { return this._view; }


//@formatter:on


	public init(): boolean
	{
        const log: kwLog = new kwLog(this.sClass, "init");
        console.log(log.called());

        if (this.bInit)
        {
            return;
        }

        this.bInit = true;

        if (!kw.isValid(this._attrs))
        {
            console.error(log.invalid("_attrs"));
            return false;
        }
        console.info(log.is("_attrs", this._attrs));


        if (!kw.isValid(this._disp))
        {
            console.error(log.invalid("_disp"));
            return false;
        }
        console.info(log.is("_disp", this._disp));


        if (!kw.isValid(this._view))
        {
            console.error(log.invalid("_view"));
            return false;
        }
        console.info(log.is("_view", this._view));


        if (!kw.isValid(this._inits))
        {
            console.error(log.invalid("_inits"));
            return false;
        }
        console.info(log.is("_inits", this._inits));

        return this.create();

	}

    private create(): boolean
    {
        const log: kwLog = new kwLog(this.sClass, "create");
        console.log(log.called());


        if (this.bCreate)
        {
            return;
        }

        this.bCreate = true;


        const attrs = this.attrs;
        if (!kw.isValid(attrs))
        {
            console.error(log.empty("attrs"));
            return false;
        }
        console.info(log.isObj("attrs"), attrs, "]");


        const disp = this.disp;
        if (!kw.isValid(disp))
        {
            console.error(log.invalid("disp"));
            return false;
        }
        console.info(log.is("disp", this.disp));


        const view = this.view;
        if (!kw.isValid(view))
        {
            console.error(log.invalid("view"));
            return false;
        }
        console.info(log.is("view", view));


        const inits = this.inits;
        if (!kw.isValid(inits))
        {
            console.error(log.invalid("inits"));
            return false;
        }
        console.info(log.is("inits", inits));


        for (let attr of attrs)
        {
            console.info(log.isObj("attr"), attr, "]");

            let item = new kwAttr(attr, disp, inits, view);
            if (!item.init())
            {
                console.error(log.errCreate("item"));
                return false;
            }
            console.info(log.isObj("item"), item, "]");

            this.items.push(item);
        }

    }



}
