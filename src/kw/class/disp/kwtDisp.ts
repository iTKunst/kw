/**********************************************************************
 *
 * kw/class/disp/kwDispType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
// @formatter:on


export class kwtDisp
{
    bDispDispAll: boolean;
    bDispDispData: boolean;
    bDispFltr: boolean;
    bDispId: boolean;
    bDispInits: boolean;
    bDispLocal: boolean;
    bDispMaster: boolean;
    bDispTag: boolean;
    bDispView: boolean;
}
