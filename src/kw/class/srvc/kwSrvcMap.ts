/**********************************************************************
 *
 * kw/class/srvc/kwSrvcMap.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {kw }                       from "@kw/kw";
import {kwMap }                    from "@kwClass/kwMap";

import {kwtSrvc }               from "./kwSrvcType";
import {kwStateX }                 from "@kwClass/kwStateX";
// @formatter:off


export class kwSrvcMap extends kwMap
{

	constructor(data: object[])
	{
		super(data);
		//console.log("kwSrvcMap::constructor() called.");
	}

	protected createMap(): boolean
	{
		//console.log("kwSrvcMap::createMap() called.");

		if (kw.isNull(this.theArr))
		{
			console.error("kwSrvcMap::init() data is invalid");
			return false;
		}

		let theArr = <kwtSrvc[]>this.theArr;

		let theMap = new Map(
			theArr.map(x => [x.sCode, x] as [string, object] )
		);
		//console.info("kwSrvcMap::createMap() theMap is ", theMap);

		this.setMap(theMap);

		return true;
	};

	xImport(record: object): object
	{
		//console.log("kwSrvcMap::ximport() called.");

		if (kw.isNull(record))
		{
			console.error("kwSrvcMap::ximport() record is invalid.");
			return
		}

		let x: kwStateX = new kwStateX(record);
		if (!x.init())
		{
			console.error("kwSrvcMap::ximport() error creating x.");
			return
		}

		let sCode: string = x.getString("sCode");
		if (!kw.isString(sCode))
		{
			console.error("kwSrvcMap::xImport() sCode is invalid.");
			return;
		}

		let sHost: string = x.getString("sHost");
		if (!kw.isString(sHost))
		{
			console.error("kwSrvcMap::xImport() sHost is invalid.");
			return;
		}

		let sProtocol: string = x.getString("sProtocol");
		if (!kw.isString(sProtocol))
		{
			console.error("kwSrvcMap::xImport() sProtocol is invalid.");
			return;
		}

		let nId: number = x.getNumber("nId");
		if (!kw.isNumber(nId))
		{
			console.error("kwSrvcMap::xImport() nId is invalid.");
			return;
		}

		let nPort: number = x.getNumber("nPort");
		if (!kw.isNumber(nPort))
		{
			console.error("kwSrvcMap::xImport() nPort is invalid.");
			return;
		}

		let type: kwtSrvc = {
			sCode: sCode,
			sHost: sHost,
			sProtocol: sProtocol,
			nId: nId,
			nPort: nPort
		};

		return type;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwSrvcMap)
	}

}
