/**********************************************************************
 *
 * kw/class/srvc/kwSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

//@formatter:off
import { kw }               from "@kw/kw";
import { kwApi }            from "@kwClass/api/kwApi";
import { kwtSrvc }          from "./kwtSrvc";
import { kwActs }           from "@kwClass/acts/kwActs";



export class kwSrvc
{

	_acts: kwActs;
	_api: kwApi;
	_apis: object;
	_type: kwtSrvc;

	_nId: number;
	_nPort: number;
	_sCode: string;
	_sHost: string;
	_sProtocol: string;
	_sSrvc: string;


	constructor(private info: object)
	{
		//console.log("kwSrvc::constructor() called");
	};


    public get nPort(): number { return this._nPort; }
    public get sCode(): string { return this._sCode; }
    public get sHost(): string { return this._sHost; }
    public get sProtocol(): string { return this._sProtocol; }

    private get acts(): kwActs { return this._acts; }
	private get api(): kwApi { return this._api; }
    private get apis(): object { return this._apis; }
    private get nId(): number { return this._nId; }
    private get sSrvc(): string { return this._sSrvc; }
    private get type(): kwtSrvc { return this._type; }

    private set acts(val: kwActs[] ) { this._acts = val; }
    private set api(val: kwApi ) { this._api = val; }
    private set apis(val: object ) { this._apis = val; }
    private set nId(val: number[] ) { this._nId = val; }
    private set nPort(val: number[] ) { this._nPort = val; }
    private set sCode(val: string ) { this._sCode = val; }
    private set sHost(val: string ) { this._sHost = val; }
    private set sProtocol(val: string ) { this._sProtocol = val; }
    private set sSrvc(val: string ) { this._sSrvc = val; }
    private set type(val: kwtSrvc ) { this._type = val; }

// @formatter:on
	init(): boolean
	{
		//console.log("kwSrvc::init() is called.");

		if (kw.isNull(this.info))
		{
			console.error("kwSrvc::init() info is invalid.");
			return false;
		}
		//console.info("kwSrvc::init() info is ", this.info);

        this.type = <kwtSrvc>thie.info;

		let sCode: string = this.type.sCode
		if(!kw.isString(sCode))
		{
			console.error("kwSrvc::init() sCode is invalid.");
			return false;
		}
		//console.info("kwSrvc::init() sCode is ", sCode);
		this.sCode = sCode;

		let sHost: string = this.type.sHost;
		if(!kw.isString(sHost))
		{
			console.error("kwSrvc::init() sHost is invalid.");
			return false;
		}
		//console.info("kwSrvc::init() sHost is ", sHost);
		this.sHost = sHost;

		let sProtocol: string = this.type.sProtocol
		if(!kw.isString(sProtocol))
		{
			console.error("kwSrvc::init() sProtocol is invalid.");
			return false;
		}
		//console.info("kwSrvc::init() sProtocol is ", sProtocol);
		this.sProtocol = sProtocol;

		let nPort: number = this.type.nPort
		if(!kw.isNumber(nPort))
		{
			console.error("kwSrvc::init() nPort is invalid.");
			return false;
		}
		//console.info("kwSrvc::init() nPort is ", nPort);
		this.nPort = nPort;

		this.sSrvc = this.sProtocol + ":" + this.sHost + ":" + this.nPort;
		//console.info("kwSrvc::init() sSrvc is ", this.sSrvc);

		return true;
	}


	toString(): string
	{
		return this.constructor.name;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwSrvc)
	}
}

