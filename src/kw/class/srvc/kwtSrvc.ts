/**********************************************************************
 *
 * kw/class/srvc/kwtSrvc.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
// @formatter:on


export class kwtSrvc
{
	nId: number;
	sCode: string;
	sHost: string;
	nPort: number;
	sProtocol: string;
}
