/**********************************************************************
 *
 * kw/class/srvcs/kwSrvcs.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {kw }               from "@kw/kw";
import {kwSrvcMap }        from "@kwClass/srvc/kwSrvcMap";
import {kwtSrvc }       from "@kwClass/srvc/kwSrvcType";
// @formatter:on

export class kwSrvcs
{
	map: kwSrvcMap;

	constructor(private list: kwtSrvc[])
	{
		//console.log("kwSrvcs::constructor() called");
	};

	init(): boolean
	{
		//console.log("kwSrvcs::init() called.");

		if(!kw.isArray(this.list) )
		{
			console.error("kwSrvcs::init() list is invalid.");
			return false;
		}
		//console.info("kwSrvcs::init() list ", this.list);

		let map: kwSrvcMap = new kwSrvcMap(this.list);
		if( !map.init() )
		{
			console.error("kwBS()::init() error creating map.");
			return false;
		}
		//console.info("kwBS()::init() map is ", map);
		this.map = map;

		return true;
	}

	getList(): kwtSrvc[]
	{
		return this.list;
	};

	getMap(): kwSrvcMap
	{
		return this.map;
	};

	toString(): string
	{
		return this.constructor.name;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwSrvcs)
	}
}

