/**********************************************************************
 *
 * kw/class/disp/kwDisp.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {kw }    		            from "@kw/kw";
import {kwDispType }			    from "./kwDispType";

const sTAG                          = "display";


export class kwDispAttrs
{

	constructor(private info: object)
	{
		//console.log("kwDisp::constructor() is called.");
	}

// @formatter:on

	init(): boolean
	{
		//console.log("kwDisp::init() is called.");

		if(kw.isNull(this.info))
		{
			console.error("kwDisp::create() info is invalid.");
			return false;
		}
		//console.info("kwDisp::create() info is [", this.info, "]");


		let disp: kwDispType = this.info[sTAG];
		if(kw.isNull(disp))
		{
			console.error("kwDisp::create() disp is invalid.");
			return false;
		}
		//console.info("kwDisp::create() disp is [", disp, "]");


        let bAll: boolean = disp.bDispAll;
        if (!kw.isBoolean(bAll))
        {
            console.error("kwDisp::create() bAll is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bAll is [", bAll, "]");
        this.bAll = bAll;


        let bArryId: boolean = disp.bDispArryId;
        if (!kw.isBoolean(bArryId))
        {
            console.error("kwDisp::create() bArryId is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bArryId is [", bArryId, "]");
        this.bArryId = bArryId;


        let bArryIdD: boolean = disp.bDispArryIdD;
        if (!kw.isBoolean(bArryIdD))
        {
            console.error("kwDisp::create() bArryIdD is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bArryIdD is [", bArryIdD, "]");
        this.bArryIdD = bArryIdD;


        let bData: boolean = disp.bDispData;
        if (!kw.isBoolean(bData))
        {
            console.error("kwDisp::create() bData is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bData is [", bData, "]");
        this.bData = bData;


        let bDataFltr: boolean = disp.bDispDataFltr;
        if (!kw.isBoolean(bDataFltr))
        {
            console.error("kwDisp::create() bDataFltr is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bDataFltr is [", bDataFltr, "]");
        this.bDataFltr = bDataFltr;


        let bFltr: boolean = disp.bDispFltr;
        if (!kw.isBoolean(bFltr))
        {
            console.error("kwDisp::create() bFltr is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bFltr is [", bFltr, "]");
        this.bFltr = bFltr;


        let bFltrIdD: boolean = disp.bDispFltrIdD;
        if (!kw.isBoolean(bFltrIdD))
        {
            console.error("kwDisp::create() bFltrIdD is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bFltrIdD is [", bFltrIdD, "]");
        this.bFltrIdD = bFltrIdD;


        let bId: boolean = disp.bDispId;
        if (!kw.isBoolean(bId))
        {
            console.error("kwDisp::create() bId is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bId is [", bId, "]");
        this.bId = bId;


        let bInits: boolean = disp.bDispInits;
		if (!kw.isBoolean(bInits))
		{
			console.error("kwDisp::create() bInits is invalid.");
			return false;
		}
		//console.info("kwDisp::create() bInit is [", bInit, "]");
		this.bInits = bInits;


        let bLink: boolean = disp.bDispLink;
        if (!kw.isBoolean(bLink))
        {
            console.error("kwDisp::create() bLink is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bLink is [", bLink, "]");
        this.bLink = bLink;


        let bLinkId: boolean = disp.bDispLinkId;
        if (!kw.isBoolean(bLinkId))
        {
            console.error("kwDisp::create() bLinkId is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bLinkId is [", bLinkId, "]");
        this.bLinkId = bLinkId;

        let bLinkIdD: boolean = disp.bDispLinkIdD;
        if (!kw.isBoolean(bLinkIdD))
        {
            console.error("kwDisp::create() bLinkIdD is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bLinkIdD is [", bLinkIdD, "]");
        this.bLinkIdD = bLinkIdD;


        let bLinkTmpl: boolean = disp.bDispLinkTmpl;
        if (!kw.isBoolean(bLinkTmpl))
        {
            console.error("kwDisp::create() bLinkTmpl is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bLinkTmpl is [", bLinkTmpl, "]");
        this.bLinkTmpl = bLinkTmpl;


        let bLocal: boolean = disp.bDispLocal;
        if (!kw.isBoolean(bLocal))
        {
            console.error("kwDisp::create() bLocal is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bLocal is [", bLocal, "]");
        this.bLocal = bLocal;


        let bMaster: boolean = disp.bDispMaster;
        if (!kw.isBoolean(bMaster))
        {
            console.error("kwDisp::create() bMaster is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bMaster is [", bMaster, "]");
        this.bMaster = bMaster;


        let bStyl: boolean = disp.bDispStyl;
		if (!kw.isBoolean(bStyl))
		{
			console.error("kwDisp::create() bStyl is invalid.");
			return false;
		}
		//console.info("kwDisp::create() bStyl is [", bStyl, "]");
		this.bStyl = bStyl;


        let bStylId: boolean = disp.bDispStylId;
        if (!kw.isBoolean(bStylId))
        {
            console.error("kwDisp::create() bStylId is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bStylId is [", bStylId, "]");
        this.bStylId = bStylId;


        let bStylIdD: boolean = disp.bDispStylIdD;
        if (!kw.isBoolean(bStylIdD))
        {
            console.error("kwDisp::create() bStylIdD is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bStylIdD is [", bStylIdD, "]");
        this.bStylIdD = bStylIdD;


        let bTag: boolean = disp.bDispTag;
		if (!kw.isBoolean(bTag))
		{
			console.error("kwDisp::create() bTag is invalid.");
			return false;
		}
		//console.info("kwDisp::create() bTag is [", bTag, "]");
		this.bTag = bTag;


		let bText: boolean = disp.bDispText;
		if (!kw.isBoolean(bTag))
		{
			console.error("kwDisp::create() bText is invalid.");
			return false;
		}
		//console.info("kwDisp::create() bText is [", bTag, "]");
		this.bText = bText;


        let bTextId: boolean = disp.bDispTextId;
        if (!kw.isBoolean(bTextId))
        {
            console.error("kwDisp::create() bTextId is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bTextId is [", bTextId, "]");
        this.bTextId = bTextId;


        let bTextIdD: boolean = disp.bDispTextIdD;
        if (!kw.isBoolean(bTextIdD))
        {
            console.error("kwDisp::create() bTextIdD is invalid.");
            return false;
        }
        //console.info("kwDisp::create() bTextIdD is [", bTextIdD, "]");
        this.bTextIdD = bTextIdD;


        let bView: boolean = disp.bDispView;
		if (!kw.isBoolean(bView))
		{
			console.error("kwDisp::create() bView is invalid.");
			return false;
		}
		//console.info("kwDisp::create() bView is [", bView, "]");
		this.bView = bView;


		return true;
	}

    dispAll(): boolean
    {
        return this.bAll;
    }

    dispArryId(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bArryId;
    }

    dispArryIdD(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bArryIdD;
    }

    dispData(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bData;
    }

    dispDataFltr(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bDataFltr;
    }

    dispFltr(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bFltr;
    }

    dispFltrIdD(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bFltrIdD;
    }

    dispId(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bId;
    }

    dispInits(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bInits;
    }

    dispLink(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bLink;
    }

    dispLinkId(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bLinkId;
    }

    dispLinkIdD(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bLinkId;
    }

    dispLinkTmpl(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bLinkTmpl;
    }

    dispLocal(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bLocal;
    }

    dispMaster(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bMaster;
    }

    dispStyl(): boolean
	{
        if (this.bAll)
        {
            return true;
        }
		return this.bStyl;
	}

    dispStylId(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bStylId;
    }

    dispStylIdD(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bStylIdD;
    }

    dispTag(): boolean
	{
        if (this.bAll)
        {
            return true;
        }
		return this.bTag;
	}

	dispText(): boolean
	{
        if (this.bAll)
        {
            return true;
        }
		return this.bText;
	}

    dispTextId(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bTextId;
    }

    dispTextIdD(): boolean
    {
        if (this.bAll)
        {
            return true;
        }
        return this.bTextIdD;
    }

    dispView(): boolean
	{
        if (this.bAll)
        {
            return true;
        }
		return this.bView;
	}

	toString(): string
	{
		return this.constructor.name;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwDisp)
	}

}
