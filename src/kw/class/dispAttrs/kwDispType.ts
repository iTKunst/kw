/**********************************************************************
 *
 * kw/class/disp/kwDispType.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
// @formatter:on


export class kwDispType
{
    bDispAll: boolean;
    bDispArryId: boolean;
    bDispArryIdD: boolean;
    bDispData: boolean;
    bDispDataFltr: boolean;
    bDispFltr: boolean;
    bDispFltrId: boolean;
    bDispFltrIdD: boolean;
    bDispId: boolean;
    bDispInits: boolean;
    bDispLink: boolean;
    bDispLinkId: boolean;
    bDispLinkIdD: boolean;
    bDispLinkTmpl: boolean;
    bDispLocal: boolean;
    bDispMaster: boolean;
    bDispStyl: boolean;
    bDispStylId: boolean;
    bDispStylIdD: boolean;
	bDispTag: boolean;
    bDispText: boolean;
    bDispTextId: boolean;
    bDispTextIdD: boolean;
	bDispView: boolean;
}
