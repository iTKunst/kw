/**********************************************************************
 *
 * kw/class/routes/kwRoutes.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import { kw }               from "@kw/kw";
import { kwRouteMap }       from "@kwClass/route/kwRouteMap";
import { kwtRoutes }        from "@kwClass/routes/kwtRoutes";



export class kwRoutes
{
	protected sClass: string = this.constructor.name;

	_sLogin:    string;
	_sLogout:   string;
	_sMain:     string;
	_sRegister: string;
	_sReset:    string;

	_type:      kwtRoutes;


	constructor(private _data: object)
	{
        const log: kwLog = new kwLog(this.sClass, "constructor");
        console.log(log.called());
	};


    public get sLogin(): string             { return this._sLogin; }
    public get sLogout(): string            { return this._sLogout; }
    public get sMain(): string              { return this._sMain; }
    public get sRegister(): string          { return this._sRegister; }
    public get sReset(): string             { return this._sReset; }

    private get data(): object              { return this._data; }
    private get type(): kwtRoutes           { return this._type; }

    private set sLogin(val: string )        { this._sLogin = val; }
    private set sLogout(val: string )       { this._sLogout = val; }
    private set sMain(val: string )         { this._sMain = val; }
    private set sRegister(val: string )     { this._sRegister = val; }
    private set sReset(val: string )        { this._sReset = val; }
    private set type(val: kwtRoutes )       { this._type = val; }

//@formatter:on

    init(): boolean
	{
		//console.log("kwRoutes::init() called.");

		if(kw.isNull(this.data) )
		{
            console.error(log.invalid("data"));
			return false;
		}
        console.info(log.is("data", this.data));

        this.type = <kwtRoutes>this.data;

		let sLogin: string = this.type.sLogin;
		if (!kw.isString(sLogin) || sLogin.length === 0)
		{
            console.error(log.invalid("sLogin"));
			return false;
		}
        console.info(log.is("sLogin", this.sLogin));
		this.sLogin = sLogin;


		let sLogout: string = this.type.sLogout
		if (!kw.isString(sLogout) || sLogout.length === 0)
		{
            console.error(log.invalid("sLogout"));
			return false;
		}
        console.info(log.is("sLogout", this.sLogout));
		this.sLogout = sLogout;


		let sMain: string = this.type.sMain
		if (!kw.isString(sMain))
		{
            console.error(log.invalid("sMain"));
			return false;
		}
        console.info(log.is("sMain", this.sMain));
		this.sMain = sMain;


		let sRegister: string = this.type.sRegister
		if (!kw.isString(sRegister) || sRegister.length === 0)
		{
            console.error(log.invalid("sRegister"));
			return false;
		}
        console.info(log.is("sRegister", this.sRegister));
		this.sRegister = sRegister;


		let sReset: string = this.type.sReset
		if (!kw.isString(sReset) || sReset.length === 0)
		{
            console.error(log.invalid("sReset"));
			return false;
		}
        console.info(log.is("sReset", this.sReset));
		this.sReset = sReset;

		return true;
	}


	toString(): string
	{
		return this.constructor.name;
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwRoutes)
	}
}

