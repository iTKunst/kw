
/**********************************************************************
 *
 * kw/class/mdl/kwMdls.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {kw }		            from "@kw/kw";
import {kwParam }              from "@kwClass/param/kwParam";
import {kwParamType }		    from "@kwClass/param/kwParamType";

import {kwMdlEnum }		    from "./kwMdlEnum";
import {kwMdlSrvc }		    from "./kwMdlSrvc";
import {kwMdlType }		    from "./kwMdlType";
// @formatter:on

export abstract class kwMdl
{

	params: kwParam[] = new Array();

	constructor(    private nType: kwMdlEnum,
	                private type: kwMdlType   )
	{
		//console.log("kwMdl::constructor() is called.");
	}

	abstract createParam(param: kwParamType): kwParam

	init(): boolean
	{
		//console.log("kwMdl::init() called.");

		if (!kw.isArray(this.params))
		{
			console.error("kwMdl::init() params is not valid.");
			return false;
		}

		if (!kwMdlSrvc.in(this.nType))
		{
			console.error("kwMdl::init() nType is not valid.");
			return false;
		}
		//console.info("kwMdl::init() nType is ", this.nType);

		if (kw.isNull(this.type))
		{
			console.error("kwMdl::init() type is not valid.");
			return false;
		}
		//console.info("kwMdl::init() type is ", this.type);

		let params: kwParamType[] = this.type.params;
		if( kw.isNull(params))
		{
			console.error("kwMdl::init() params is not valid.");
			return false;
		}
		//console.info("kwMdl::init() params is ", params);

		if (!this.createParams(params))
		{
			console.error("kwMdl::init() error creating params.");
			return false;
		}

		return true;
	}

	copyRecord()
	{
		//console.log("kwMdl::copyRecord() called.");

		if (!kw.isArray(this.params))
		{
			console.error("kwMdl::copyRecord() params is not valid.");
			return;
		}
		//console.info("kwMdl::copyRecord() params is ", this.params);

		let record: object;

		for (let i=0; i<this.params.length; i++)
		{
			let param = this.params[i];
			if (kw.isNull(param))
			{
				console.error("kwMdl::copyRecord() param is not valid.");
				return;
			}
			//console.info("kwMdl::copyRecord() param is ", param);

			param.addDefault(record);
		}

		//console.info("kwMdl::copyRecord() record is ", record);

		return record;
	}

	createRecord()
	{
		//console.log("kwMdl::createRecord() called.");
	
		if (!kw.isArray(this.params))
		{
			console.error("kwMdl::createRecord() params is not valid.");
			return;
		}
		//console.info("kwMdl::createRecord() params is ", this.params);
	
		let record: object;
	
		for (let i=0; i<this.params.length; i++)
		{
			let param = this.params[i];
			if (kw.isNull(param))
			{
				console.error("kwMdl::createRecord() param is not valid.");
				return;
			}
			//console.info("kwMdl::createRecord() param is ", param);
	
			param.addDefault(record);
		}
	
		//console.info("kwMdl::createRecord() record is ", record);
	
		return record;
	}

	createParams(params): boolean
	{
		//console.log("kwMdl::createParams() called.");
	
		if (!kw.isArray(params))
		{
			console.error("kwMdl::createParams() params is not valid.");
			return;
		}
		//console.info("kwMdl::createParams() params is ", params);

		for (let i=0; i<params.length; i++)
		{
			let param = params[i];
			if (kw.isNull(param))
			{
				console.error("kwMdl::createParams() param is not valid.");
				return false;
			}
			//console.info("kwMdl::createParams() param is ", param);
			//console.info("kwMdl::createParams() params is ", this.params);
	
			let obj = this.createParam(param);
			if (kw.isNull(obj))
			{
				console.error("kwMdl::createParams() obj is not valid.");
				return false;
			}
			//console.info("kwMdl::createParams() obj is ", obj);
	
			this.params.push(obj);
		}
	
		//console.info("kwMdl::createParams() params is ", this.params);

		return true;
	}

	toString(): string
	{
		return kw.toString(this.nType, kwMdlEnum);
	};

	static is(obj: object): boolean
	{
		return kw.is(obj, kwMdl)
	}

}
