/**********************************************************************
 *
 * kw/class/mdl/kwMdlFcty.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/

// @formatter:off
import {kw }		            from "@kw/kw";
import {kwMap }                from "@kwClass/kwMap";
import {kwMdl }                from "./kwMdl";
import {kwMdlFull }            from "./kwMdlFull";
import {kwMdlEnum }		    from "./kwMdlEnum";
import {kwMdlSub }             from "./kwMdlSub";
import {kwMdlSrvc }            from "./kwMdlSrvc";
import {kwMdlType }            from "./kwMdlType";
// @formatter:on


const sPARAMS: string = "params";
const sTYPE: string = "sType";

export class kwMdlFcty
{
	static create(
		info: kwMdlType,
		currs: kwMap,
		langs: kwMap,
		tZs: kwMap  ): kwMdl
	{
		//console.log("kwMdl::create() called");

		if (kw.isNull(info))
		{
			console.error("kwMdl::create() info is invalid");
			return;
		}
		//console.info("kwMdl::create() info is ", info);

		if (!kwMap.is(currs))
		{
			console.error("kwMdl::create() currs is invalid");
			return;
		}
		//console.info("kwMdl::create() currs is ", currs);

		if (!kwMap.is(langs))
		{
			console.error("kwMdl::create() langs is invalid");
			return;
		}
		//console.info("kwMdl::create() langs is ", langs);

		if (!kwMap.is(tZs))
		{
			console.error("kwMdl::create() tZs is invalid");
			return;
		}
		//console.info("kwMdl::create() tZs is ", tZs);

		let sMdl: string = info[sTYPE];
		if(!kw.isString(sMdl))
		{
			console.error("kwMdl()::create() sMdl is invalid.");
			return;
		}
		//console.info("kwMdl()::create() sMdl is ", sMdl);

		let params: object[] = info[sPARAMS];
		if(!kw.isArray(params))
		{
			console.error("kwMdl()::create() params is invalid.");
			return;
		}
		//console.info("kwMdl()::create() params is ", params);

		let nMdl: kwMdlEnum = kwMdlSrvc.toEnum(sMdl);
		if (!kwMdlSrvc.in(nMdl))
		{
			console.error("kwMdl()::create() nMdl is invalid.");
			return;
		}

		let mdl: kwMdl;

		switch(nMdl)
		{
			case kwMdlEnum.full:
			{
				mdl = new kwMdlFull(info, currs, tZs, langs);
				break;
			}

			case kwMdlEnum.sub:
			{
				mdl = new kwMdlSub(info);
				break;
			}

			default:
			{
				console.error("kwMdl()::create() nMdl is invalid.");
				return;
			}
		}

		if (!kwMdl.is(mdl))
		{
			console.error("kwMdl()::create() error creating mdl.");
			return;
		}

		if (!mdl.init())
		{
			console.error("kwMdl()::create() error initializing mdl.");
			return;
		}
		//console.info("kwMdl()::create() mdl is ", mdl);

		return mdl;
	}
}

