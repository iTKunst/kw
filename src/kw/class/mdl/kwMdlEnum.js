"use strict";
/**********************************************************************
 *
 * kw/enum/kwMdlEnum.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst Corporation
 *
 **********************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
var kwMdlEnum;
(function (kwMdlEnum) {
    kwMdlEnum[kwMdlEnum["Full"] = 1] = "Full";
    kwMdlEnum[kwMdlEnum["Sub"] = 2] = "Sub";
})(kwMdlEnum = exports.kwMdlEnum || (exports.kwMdlEnum = {}));
