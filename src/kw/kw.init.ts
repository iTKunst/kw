/**********************************************************************
 *
 * kwNg/kw.init.ts
 *
 * author: Patrick Dooley
 *
 *
 **********************************************************************
 *
 * Copyright (c) 2017 iTKunst corporation
 *
 **********************************************************************/

// @formatter:off
import { Http }                 from "@angular/http";
import { Injectable }           from "@angular/core";
import 'rxjs/add/operator/map';

import { kw }                   from "../kw/kw";
import { kwApis }               from "./class/apis/kwApis";
import { kwBS }                 from "../kw/class/BS/kwBS";


const sURL: string = 'assets/bootstrap.json';
// @formatter:on

@Injectable()
export class kwInit
{

	bootstrap: kwBS;

	constructor(private http: Http)
	{
		//console.log("kwInit::constructor() called.");
	}

	getApis(sCode: string): kwApis
	{
		return this.bootstrap.getApis();
	}

	getBS(): kwBS
	{
		return this.bootstrap;
	}

	load(): Promise<any>
	{
		//console.log("kwInit::load() called.");

		let promise: Promise<any> = new Promise((resolve: any) =>
		{
			this.http.get(sURL).map(res => res.json())
				.subscribe(config => {
					resolve(this.store(config));
				});
		});
		return promise;
	}

	store(config: any): boolean
	{
		//console.log("kwInit::store() called.");

		if (kw.isNull(config))
		{
			console.error("kwInit()::store() config is invalid.");
			return false;
		}

		this.bootstrap = new kwBS(config);
		if (!this.bootstrap.init())
		{
			console.error("kwInit()::store() error initializing bootstrap.");
			return false;
		}

		return true;
	}
}

export function initConfig(configService: kwInit): Function {
	return () => configService.load();
}